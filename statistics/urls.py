from django.conf.urls import url
from statistics import views

app_name = "statistics"
urlpatterns = [
	#dashboard
	url(r'^statistics/$', views.statistics, name='statistics'),
	url(r'^statistics/load_cards/$', views.load_cards, name='load_cards'),
	url(r'^statistics/load_chart_hours_blocks/$', views.load_chart_hours_blocks, name='load_chart_hours_blocks'),
	url(r'^statistics/load_chart_doors/$', views.load_chart_doors, name='load_chart_doors'),
	url(r'^statistics/load_detail_table/$', views.load_detail_table, name='load_detail_table'),
	url(r'^statistics/filter_mall_detail_table/$', views.filter_mall_detail_table, name='filter_mall_detail_table'),
	url(r'^statistics/filter_door_detail_table/$', views.filter_door_detail_table, name='filter_door_detail_table'),
	#user
	url(r'^statistics/user/$', views.user, name='user'),
	url(r'^statistics/user/form_user/$', views.form_user, name='form_user'),
	url(r'^statistics/user/filter_user/$', views.filter_user, name='filter_user'),
	url(r'^statistics/user_update/([0-9]+)/$', views.user_update, name='user_update'),
	url(r'^statistics/user_status/([0-9]+)/([0-9]+)/$', views.user_status, name='user_status'),
	#networking
	url(r'^statistics/networking/$', views.networking, name='networking'),
	url(r'^statistics/grafic/$', views.grafic, name='grafic'),
	url(r'^statistics/report_networking/$', views.report_networking, name='report_networking'),
	#Camera
	url(r'^statistics/cameras/$', views.cameras, name='cameras'),
	url(r'^statistics/modal/$', views.modal, name='modal'),
	#Mapas
	url(r'^statistics/maps/$', views.maps, name='maps'),
	#Crontab
	url(r'^statistics/cron/$', views.cron, name='cron'),
	url(r'^statistics/cron/filter_cron/$', views.filter_cron, name='filter_cron'),
	url(r'^statistics/new_cron/$', views.new_cron, name='new_cron'),
	url(r'^statistics/cron_update/([0-9]+)/$', views.cron_update, name='cron_update'),
	url(r'^statistics/cron_delete/([0-9]+)/$', views.cron_delete, name='cron_delete'),
	#Funciones para apagar y encender camaras
	url(r'^statistics/protocolo/$', views.protocolo, name='protocolo'),
	url(r'^statistics/protocolo_lote/$', views.protocolo_lote, name='protocolo_lote'),
	url(r'^statistics/activate/$', views.activate, name='activate'),
	url(r'^statistics/activate_lote/$', views.activate_lote, name='activate_lote'),


]

