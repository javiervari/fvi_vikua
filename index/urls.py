from django.conf.urls import patterns, url
from django.conf.urls.static import static
from index import views
from django.conf import settings

app_name = "index"
urlpatterns = [
    url(r'^$', views.index),
    url(r'^destroy_session/$', views.destroy_session, name='destroy_session'),
]

