$( document ).ready(function() {
    $.ajax({
    url:   '../statistics/load_cards/',
    cache: false,
    beforeSend: function(){
                $('#chartdiv').html("<div class='col-xs-16'><div class='load1 load-wrapper'><div class='loader'></div></div></div>");
                },
    success:  function (response) {
    	var tolon_i = $("#el-tolon-entry")
    	var tolon_o = $("#el-tolon-exit")
    	var tolon_rel = $("#el-tolon-rel")

    	var hatillo_i = $("#paseo-el-hatillo-entry")
    	var hatillo_o = $("#paseo-el-hatillo-exit")
    	var hatillo_rel = $("#paseo-el-hatillo-rel")

    	var llano_i = $("#llano-mall-entry")
    	var llano_o = $("#llano-mall-exit")
    	var llano_rel = $("#llano-mall-rel")

    	var obj = JSON.parse(response);

        var data_before = obj.data_before
        var data_now = obj.data_now

        var relacion

    	// FORMULA es relation = ((entry/entry_before) - 1 ) *100

        tolon_i.text(data_now[0].entry)
        tolon_o.text(data_now[0].exit)
        tolon_rel.attr('title', 'Día anterior '+data_before[0].entry_before).tooltip()

        if (data_before[0].entry_before != 0){
            relacion = (((data_now[0].entry / data_before[0].entry_before)-1)*100 ).toFixed(2)
        }
        else{
            relacion = 0
        }

        if (relacion > 0){
            tolon_rel.text(relacion.toString()+"%");
            tolon_rel.append('<i class="fa fa-arrow-up toup"></i>')
        }else if (relacion<0){
            tolon_rel.text(relacion.toString()+"%");
            tolon_rel.append('<i class="fa fa-arrow-down todown"></i>')
        }else{
            tolon_rel.text(relacion.toString()+"%");
            tolon_rel.append('<i class="fa fa-circle equal"></i>')
        }




        hatillo_i.text(data_now[1].entry)
        hatillo_o.text(data_now[1].exit)
        hatillo_rel.attr('title', 'Día anterior '+data_before[1].entry_before).tooltip()

        if (data_before[1].entry_before != 0){
            relacion = (((data_now[1].entry / data_before[1].entry_before)-1)*100 ).toFixed(2)
        }
        else{
            relacion=0
        }


        if (relacion > 0){
            hatillo_rel.text(relacion.toString()+"%");
            hatillo_rel.append('<i class="fa fa-arrow-up toup"></i>')
        }else if (relacion<0){
            hatillo_rel.text(relacion.toString()+"%");
            hatillo_rel.append('<i class="fa fa-arrow-down todown"></i>')
        }else{
            hatillo_rel.text(relacion.toString()+"%");
            hatillo_rel.append('<i class="fa fa-circle equal"></i>')
        }


        llano_i.text(data_now[2].entry)
        llano_o.text(data_now[2].exit)
        llano_rel.attr('title', 'Día anterior '+data_before[2].entry_before).tooltip()

        if (data_before[2].entry_before != 0){
            relacion = (((data_now[2].entry / data_before[2].entry_before)-1)*100 ).toFixed(2)
        }
        else{
            relacion=0
        }


        if (relacion > 0){
            llano_rel.text(relacion.toString()+"%");
            llano_rel.append('<i class="fa fa-arrow-up toup"></i>')
        }else if (relacion<0){
            llano_rel.text(relacion.toString()+"%");
            llano_rel.append('<i class="fa fa-arrow-down todown"></i>')
        }else{
            llano_rel.text(relacion.toString()+"%");
            llano_rel.append('<i class="fa fa-circle equal"></i>')
        }


        // llano_i.text(data_now[2].entry)
        // llano_o.text(data_now[2].exit)
        // llano_rel.text("0")


    }
});
});
