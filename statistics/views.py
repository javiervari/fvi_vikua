# -*- coding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.shortcuts import render, redirect
from django.db.models import Q, Max, Sum, Count, F
from django.http import HttpResponseRedirect, HttpResponse, HttpRequest, JsonResponse
from django.db import connections
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from django.core.exceptions import PermissionDenied
from django.template import loader
from django.core import serializers

from datetime import datetime, timedelta, date
import copy
import time
import calendar
import string, os, sys, random, smtplib, json, xlwt
from functions import  f, ip, ruta
from index.models import Role, User , UserDetail

from statistics.models import Camera, Company, Door, Dynamic, EventLog, Mall, Cron, MallMaps, Hour, ViewDetail

try:
    from hashlib import sha1 as _sha, md5 as _md5
except ImportError:
    import sha
    import md5ViewDetail
    _sha = sha.new
    _md5 = md5.new

from django.db.models import Avg
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from crontab import CronTab
from relay import main

def statistics(request):
	if not 'role_id' in request.session:
		return redirect('/')
	if request.session['role_id'] == 2 or request.session['role_id'] == 1:
		#Aqui defino el id del mall segun el rol del usuario que esta logueado.
		if request.session['role_id'] == 2:
			search_mall = UserDetail.objects.filter(user_id=request.session['user_id'])
			mall = search_mall[0].mall_id
			query_mall = Mall.objects.filter(active=True, id=mall).order_by('id')
			mall_id = str(mall)
		else:
			query_mall = Mall.objects.filter(active=True).order_by('id')
			mall_id = ""

		list = []
		for i in query_mall:
			i = {'mall_id': i.id,
				 'mall_name': i.mall_name
				 }
			list += [i]
		return render(request, "statistics/dashboard.html", locals())
	else:
		raise PermissionDenied




def load_cards(request):
	data_before = []
	data_now = []

	#Si son mas de las 10:00am
	if datetime.now().strftime("%H:%m:%s") > "10:00:00":
		#Establecer rangos de HOY Y AYER
		date_start = datetime.now().strftime("%Y-%m-%d 10:00:00-00") 
		date_end = datetime.now().strftime("%Y-%m-%d %H:%M:%S-00")

		date_before_start = datetime.strftime(date.today() - timedelta(1),'%Y-%m-%d 10:00:00-00')
		date_before_end = datetime.strftime(date.today() - timedelta(1),'%Y-%m-%d ')+datetime.now().strftime("%H:%M:%S-00")
	else:
		#Establecer rangos de AYER Y ANTES DE AYER
		date_start = datetime.strftime(date.today() - timedelta(1),'%Y-%m-%d 10:00:00-00')
		date_end = datetime.strftime(date.today() - timedelta(1),'%Y-%m-%d 23:59:59-00')

		date_before_start = datetime.strftime(date.today() - timedelta(2),'%Y-%m-%d 10:00:00-00')
		date_before_end = datetime.strftime(date.today() - timedelta(2),'%Y-%m-%d 23:59:59-00')

	for i in Mall.objects.all():
		qs_today = Dynamic.objects.filter(time__range=(date_start, date_end),
										  camera_id__door__mall__id=i.id)\
								  .aggregate(entry=Sum('entry'),
								  			 exit=Sum('exit'))

		qs_before = Dynamic.objects.filter(time__range=(date_before_start, date_before_end),
										   camera_id__door__mall__id=i.id)\
								   .aggregate(entry_before=Sum('entry'),
								   			  exit_before=Sum('exit'))

		data_before += [
			{'entry_before':int(qs_before['entry_before']) if qs_before['entry_before'] else 0,
			'exit_before':int(qs_before['exit_before']) if qs_before['exit_before'] else 0,
			'mall_id':i.id,}
			]

		data_now += [
			{'entry':int(qs_today['entry']) if qs_today["entry"] else 0,
			'exit':int(qs_today['exit']) if qs_today["exit"] else 0,
			'mall_id':i.id,}
			]

	data = {"data_now": data_now, "data_before": data_before}

	data = json.dumps(data)
	return HttpResponse(data)



@csrf_exempt
def load_chart_hours_blocks(request):

	if not request.POST:
		dynamic = "entry"
	else:
		dynamic = request.POST["dynamic"]

	json_data = []


	if datetime.strftime(datetime.now(),'%H:%M:%S') < '10:00:00':
		now = datetime.strftime(date.today() - timedelta(1),'%Y-%m-%d')
	else:
		now = datetime.strftime(date.today(),'%Y-%m-%d')
	
	for i in range(10,24): #Rango de horas desde las 10 hasta las 23
		element = {'hour':str(i)+':00:00'} #HH:00:00
		range1 = now+" "+str(i)+':00:00-00'
		range2 = now+" "+str(i)+':59:59-00'

		qs = Dynamic.objects.filter(time__range=(str(range1),str(range2)))\
								.values('camera_id__door__mall__mall_name')\
								.annotate(entry = Sum('entry'),exit=Sum('exit'))

		if qs:
			for k in qs:
				if dynamic == 'entry':
					element[str(k['camera_id__door__mall__mall_name'])] = int(k['entry'])
				else:
					element[str(k['camera_id__door__mall__mall_name'])] = int(k['exit'])
			json_data.append(element)

	return HttpResponse(json.dumps(json_data))


@csrf_exempt
def load_chart_doors(request):
	if datetime.strftime(datetime.now(),'%H:%M:%S') < '10:00:00':
		range1 = datetime.strftime(date.today() - timedelta(1),'%Y-%m-%d 10:00:00-00')
		range2 = datetime.strftime(date.today() - timedelta(1),'%Y-%m-%d 23:59:59-00')
	else:
		range1 = datetime.now().strftime("%Y-%m-%d 10:00:00-00") 
		range2 = datetime.now().strftime("%Y-%m-%d %H:%M:%S-00")

	if not request.POST: #GET
		mall_id = 1 if request.session["role_id"] == 1 else request.session["mall_id"]
	else: #POST
		mall_id = int(request.POST['mall_id'])

	qs = Dynamic.objects.filter(time__range = (range1,range2),
							camera_id__door__mall__id=mall_id)\
					.values('camera_id__door__door_name', 'camera_id__door__mall__mall_name')\
					.annotate(entry = Sum('entry'),exit=Sum('exit'))

	if mall_id == 1:
		colors = ['#024930', '#009e60', '#1eb53a', '#007a3d', '#5bbf21']
	elif mall_id == 2:
		colors = ['#356AC8', '#132648', '#7697D1', '#293448', '#274F95']
	elif mall_id == 3:
		colors = ['#622200', '#EF990C', '#F93C0C', '#F85600', '#F9810C']


	select = copy.copy(colors)
	select_exit = copy.copy(colors)
	entry = []
	exits = []	

	if qs:
		for data in qs:
			if not select: select=copy.copy(colors)
			color = select[0]
			del select[0]
			result = {'label':str(data['camera_id__door__door_name'])+" <b>"+str(data['entry'])+"</b>",
			'data':int(data['entry']),
			'color':color,
			}
			entry += [result]

		for data in qs:
			if not select_exit: select_exit=copy.copy(colors)
			color = select_exit[0]
			del select_exit[0]
			result = {'label':str(data['camera_id__door__door_name'])+" <b>"+str(data["exit"])+"</b>",
			'data':int(data["exit"]),
			'color':color,
			}
			exits += [result]
	

	json_return = json.dumps({'data_entry': entry, 'data_exit': exits})
	return HttpResponse(json_return)


@csrf_exempt
def load_detail_table(request):

	blocks = ["10:00:00","11:00:00","12:00:00","13:00:00",
		"14:00:00","15:00:00","16:00:00","17:00:00",
		"18:00:00","19:00:00","20:00:00","21:00:00",
		"22:00:00","23:00:00"]

	if datetime.strftime(datetime.now(),'%H:%M:%S') < '10:00:00':

		now = datetime.strftime(date.today() - timedelta(1),'%Y-%m-%d')
	else:
		now = datetime.strftime(date.today(),'%Y-%m-%d')
		
	json_table = []
	json_mall = []
	json_door = []

	for i in blocks:
		hour = i[:-3]
		time_block = str(now+" "+i+"-00")
		if request.session["role_id"] == 1:
			query_view = ViewDetail.objects.filter(time=time_block).values('time').aggregate(sum_entry=Sum('sum_entry'), sum_exit = Sum('sum_exit'))
		else:
			query_view = ViewDetail.objects.filter(time=time_block, mall_id=request.session['mall_id']).values('time').aggregate(sum_entry=Sum('sum_entry'), sum_exit = Sum('sum_exit'))
		if query_view['sum_entry'] != None and query_view['sum_exit'] != None:
			result = {'date': now,
			'time': hour,
			'sum_entry': str(query_view['sum_entry']),
			'sum_exit': str(query_view['sum_exit']),
			}
			json_table += [result]

	if request.session["role_id"] == 1:
		query_mall = Mall.objects.filter(active=True).values_list("id","mall_name")
		for data in query_mall:
			result_mall = {
			'mall_id': data[0],
			'mall_name': data[1]
			}
			json_mall += [result_mall]
		return HttpResponse(json.dumps({'table': json_table, 'mall': json_mall}))
	else:
		query_mall = Mall.objects.filter(active=True, id=request.session['mall_id']).values_list("id","mall_name")
		for data in query_mall:
			result_mall = {
			'mall_id': data[0],
			'mall_name': data[1]
			}
			json_mall += [result_mall]

		query_door = Door.objects.filter(mall_id=request.session["mall_id"]).values_list("id","door_name")
		for data in query_door:
		 	result_door = {
			'door_id': data[0],
		 	'door_name': data[1]
		 	}
	 		json_door += [result_door]

		return HttpResponse(json.dumps({'table': json_table, 'mall': json_mall, 'door': json_door}))


@csrf_exempt
def filter_mall_detail_table(request):

	blocks = ["10:00:00","11:00:00","12:00:00","13:00:00",
		"14:00:00","15:00:00","16:00:00","17:00:00",
		"18:00:00","19:00:00","20:00:00","21:00:00",
		"22:00:00","23:00:00"]

	if datetime.strftime(datetime.now(),'%H:%M:%S') < '10:00:00':

		now = datetime.strftime(date.today() - timedelta(1),'%Y-%m-%d')
	else:
		now = datetime.strftime(date.today(),'%Y-%m-%d')

	json_table = []
	json_door = []

	for i in blocks:
		hour = i[:-3]
		time_block = str(now+" "+i+"-00")

		if request.POST['mall'] == "0":
			query_view = ViewDetail.objects.filter(time=time_block).values('time').aggregate(sum_entry=Sum('sum_entry'), sum_exit = Sum('sum_exit'), sum_inside= Sum('inside'))
		else:
			query_view = ViewDetail.objects.filter(time=time_block, mall_id=request.POST['mall']).values('time', 'inside').aggregate(sum_entry=Sum('sum_entry'), sum_exit = Sum('sum_exit'), sum_inside= Sum('inside'))

		if query_view['sum_entry'] != None and query_view['sum_exit'] != None:
			result = {'date': now,
			'time': hour,
			'sum_entry': str(query_view['sum_entry']),
			'sum_exit': str(query_view['sum_exit']),
			'sum_inside': str(query_view['sum_inside']),
			}
			json_table += [result]

	query_door = Door.objects.filter(mall_id=request.POST["mall"]).values_list("id","door_name")
	for data in query_door:
	 	result_door = {
		'door_id': data[0],
	 	'door_name': data[1]
	 	}
	 	json_door += [result_door]

	return HttpResponse(json.dumps({'table': json_table, 'door': json_door}))


@csrf_exempt
def filter_door_detail_table(request):
	blocks = ["10:00:00","11:00:00","12:00:00","13:00:00",
		"14:00:00","15:00:00","16:00:00","17:00:00",
		"18:00:00","19:00:00","20:00:00","21:00:00",
		"22:00:00","23:00:00"]

	
	if datetime.strftime(datetime.now(),'%H:%M:%S') < '10:00:00':

		now = datetime.strftime(date.today() - timedelta(1),'%Y-%m-%d')
	else:
		now = datetime.strftime(date.today(),'%Y-%m-%d')
	
	json_table = []

	for i in blocks:
		hour = i[:-3]
		time_block = str(now+" "+i+"-00")

		if request.POST['door'] == "0":
			query_view = ViewDetail.objects.filter(time=time_block).values('time').aggregate(sum_entry=Sum('sum_entry'), sum_exit = Sum('sum_exit'), sum_inside= Sum('inside'))
		else:
			query_view = ViewDetail.objects.filter(time=time_block, door_id=request.POST['door']).values('time', 'inside').aggregate(sum_entry=Sum('sum_entry'), sum_exit = Sum('sum_exit'), sum_inside= Sum('inside'))

		if query_view['sum_entry'] != None and query_view['sum_exit'] != None:
			result = {'date': now,
			'time': hour,
			'sum_entry': str(query_view['sum_entry']),
			'sum_exit': str(query_view['sum_exit']),
			}
			json_table += [result]

	return HttpResponse(json.dumps(json_table))



def user(request):

	if not 'role_id' in request.session:
		return redirect('/')

	if request.session['role_id'] == 2 or request.session['role_id'] == 1:
		#inicializo mi variable filtro que cambiara dependiendo del rol
		filtro = {}
		if request.session['role_id'] == 2:
			search_mall = UserDetail.objects.filter(user_id=request.session['user_id'])
			mall = search_mall[0].mall_id
			#Filtro para que muestre en la templete los c.c disponibles
			query_mall = Mall.objects.filter(active=True, id=mall).order_by('id')
			# si es rol 2 este sera el filtro de la consulta
			filtro.update({"mall_id":str(mall), "user__role_id":2})
		else:
			#Filtro para que muestre en la templete los c.c disponibles
			query_mall = Mall.objects.filter(active=True,company_id=request.session['company_id']).order_by('id')


		data_users = UserDetail.objects.filter(**filtro
			).values('user_id','user__role__role_name','user__first_name','user__last_name','user__active','user__role_id'
			).annotate(Avg('user_id')).order_by('user__role_id')

		listx = []
		for data in data_users:

			users = UserDetail.objects.filter(user_id=data['user_id'])
			mall_list = []
			for x in users:
				mall_list+=[x.mall.mall_name]


			l = {'user_id':data['user_id'],
				'role_name':data['user__role__role_name'],
				'first_name':data['user__first_name'],
				'last_name':data['user__last_name'],
				'mall':mall_list,
				'active':data['user__active'],
				'role_id' : data['user__role_id'],
				}

			listx += [l]


		return render(request, "statistics/user.html", locals())
	else:
		raise PermissionDenied

@csrf_exempt
def filter_user(request):
	filtro = {}
	if request.POST['mall_id'] != 'fvi':
		filtro.update({"mall_id":request.POST['mall_id'], "user__role_id":2})


	data_users = UserDetail.objects.filter(**filtro
			).values('user_id','user__role__role_name','user__first_name','user__last_name','user__active'
			).annotate(Avg('user_id')).order_by('user__role_id')

	listx = []
	for data in data_users:

		users = UserDetail.objects.filter(user_id=data['user_id'])
		mall_list = []
		for x in users:
			mall_list+=[x.mall.mall_name]


		l = {'user_id':data['user_id'],
			'role_name':data['user__role__role_name'],
			'first_name':data['user__first_name'],
			'last_name':data['user__last_name'],
			'mall':mall_list,
			'active':data['user__active'],
			}

		listx += [l]
	return render(request, "statistics/user_list.html", locals())


def form_user(request):

	if not 'role_id' in request.session:
		return redirect('/')

	if request.session['role_id'] == 2 or request.session['role_id'] == 1:
		if not request.POST:
			if request.session['role_id'] == 1:
				company = Company.objects.filter(id=request.session['role_id']).order_by('id')
				rol = Role.objects.filter(active=True).order_by('id')
				mall = Mall.objects.filter(active=True,company_id=request.session['company_id']).order_by('id')

			elif request.session['role_id'] == 2:
				search_mall = UserDetail.objects.filter(user_id=request.session['user_id'])
				result_mall = search_mall[0].mall_id
				mall = Mall.objects.filter(active=True, id=result_mall).order_by('id')
				rol = Role.objects.filter(active=True).order_by('id').exclude(id="1")

			return render(request, "statistics/form_profile_info.html", locals())

		else:
			query_user = User.objects.filter(email=request.POST['email'],  active = True)
			if not query_user:
				q= User(
				password = str(_md5(request.POST['password']).hexdigest()),
			    email = request.POST["email"],
			    first_name = request.POST["first_name"].upper(),
			    last_name = request.POST["last_name"].upper(),
			    cellphone_number = request.POST["code"]+request.POST["cellphone"],
			    role_id = request.POST["role_id"],
			    create_time = timezone.now(),
			    create_id = request.session['user_id'],
			    active = True

				)
				q.save()
				user_id = q.id


				mall = request.POST.getlist('mall_id')
				if mall:
					for x in mall:
						q = UserDetail(
			                user_id = user_id,
							company_id = request.session['company_id'],
					   		mall_id = x,
					   		create_time = timezone.now(),
						    create_id = request.session['user_id']
			                  )
						q.save()

				messages.add_message(request, messages.SUCCESS, 'Guardado con exito')
				return redirect("/statistics/statistics/user/")

			else:
				messages.add_message(request, messages.WARNING, 'Correo ya existe')
				return render(request, "statistics/form_profile_info.html", locals())
	else:
		raise PermissionDenied


def user_update(request,user_id):

	if not 'role_id' in request.session:
		return redirect('/')

	if request.session['role_id'] == 2 or request.session['role_id'] == 1:
		users = UserDetail.objects.filter(user_id=user_id)
		if not request.POST:

			mall_list = []
			for data in users:
				mall_list+=[data.mall_id]

			i = {

				'first_name' : users[0].user.first_name,
				'last_name' : users[0].user.last_name,
				'email' : users[0].user.email,
				'role_id' : users[0].user.role_id,
				'cellphone_number' :users[0].user.cellphone_number,
				'mall':mall_list
			}

			if request.session['role_id'] == 1:
				rol = Role.objects.filter(active=True).order_by('id')
				mall = Mall.objects.filter(active=True,company_id=request.session['company_id']).order_by('id')


			elif request.session['role_id'] == 2:
				search_mall = UserDetail.objects.filter(user_id=request.session['user_id'])
				result_mall = search_mall[0].mall_id
				mall = Mall.objects.filter(active=True, id=result_mall).order_by('id')
				rol = Role.objects.filter(active=True).order_by('id').exclude(id="1")

			return render(request, "statistics/user_edit.html", locals())
		else:

			try:
				password = str(_md5(request.POST['password']).hexdigest())
			except KeyError:
				password = users[0].user.password

			User.objects.select_related().filter(id=request.POST["id"]).update(
				password = password,
			    email = request.POST["email"],
			    first_name = request.POST["first_name"].upper(),
			    last_name = request.POST["last_name"].upper(),
			    cellphone_number = request.POST["cellphone_number"],
			    role_id = request.POST["role_id"])



			mall = request.POST.getlist('mall_id')
			if mall:
				UserDetail.objects.filter(user_id=user_id).delete()
				for i in mall:
					UserDetail(user_id = request.POST["id"],
							company_id = request.session['company_id'],
					   		mall_id = i,
					   		create_time = timezone.now(),
						    create_id = request.session['user_id']).save()


			messages.add_message(request, messages.INFO, 'Usuario se ha editado')
			return redirect("/statistics/statistics/user/")
	else:
		raise PermissionDenied

def user_status(request,user_id,tipo):
	user = User.objects.filter(id=user_id)
	if tipo == "1":
		user.update(active=False)
		messages.add_message(request, messages.WARNING, 'Usuario se ha sido desactivado para la aplicacion.')
	else:
		user.update(active=True)
		messages.add_message(request, messages.INFO, 'Usuario se ha sido activado para la aplicación.')

	return redirect("/statistics/statistics/user/")



def networking(request):

	if not 'role_id' in request.session:
		return redirect('/')

	if request.session['role_id'] == 2 or request.session['role_id'] == 1:
		# Se busca el id del centro comercial depende del rol que tenga el usuario
		if request.session['role_id'] == 2:
			search_mall = UserDetail.objects.filter(user_id=request.session['user_id'])
			result = search_mall[0].mall_id
			mall = Mall.objects.filter(active=True, id=result).order_by('id')

			door = Door.objects.filter(mall_id=result)

		else:
			mall = Mall.objects.filter(active=True,company_id=request.session['company_id']).order_by('id')


		return render(request, "statistics/networking.html", locals())
	else:
		raise PermissionDenied


@csrf_exempt
def grafic(request):


	mall_id = request.POST['mall_id']

	#excepcion por si variable door viene vacia
	try:
		door_id = request.POST["door"]
	except KeyError:
		door_id = "todas"

	type_report = request.POST['type_report']
	date1 = request.POST['date1']
	date2 = request.POST['date2']
	listx = []


	# CONSOLIDADO
	if mall_id == "consolidado":

		#Hago una consulta de todos los centros comerciales activos para sacar el nombre
		query_mall = Mall.objects.filter(active=True,company_id=request.session['company_id']).order_by('id')

		x = ()

		for data in query_mall:

			i = (data.id,)

			x += (i)

		mall=x
	# Si selecciono un centro comercial en particular
	else:
		mall= '('+request.POST['mall_id']+')'

	# Si existe ambas fechas se va creando el where
	if date1 and date2:
		date_start = date1+" 00:00:00"
		date_end = date2+" 23:59:59"

		c =  "and w.time BETWEEN '"+date_start + "' AND '" +date_end+"'"

	else:
		c = ""
	# Se crear el where para puertas
	if door_id == "todas":
		d = ""
	else:

		d =  "and door.id in ("+ str(door_id)+")"

	# Se crea el where para centro comercial
	if mall:

		e =  "AND mall.id in "+str(mall)
	else:
		e = ""

	# Si selecciona entradas por anio
	if type_report == '1':

		query_report = """SELECT
							date_part('year',w.time) as anio,
							sum(w.entry) as entrada
							from statistics.dynamic w
							inner join statistics.camera on camera.id = w.camera_id
							inner join statistics.door on door.id = camera.door_id
							inner join statistics.mall on mall.id = door.mall_id
							where mall.active=True """+str(c)+""" """+str(d)+""" """+str(e)+"""
							group by date_part('year',w.time)
							order by 1,2;
							"""
		q = connections['default'].cursor()
		q.execute(query_report)
		row = q.fetchall()

		for data in row:


			l = {'x':data[0],
				'y':str(data[1]),
				}

			listx += [l]

	# Si selecciona entada por mes
	elif type_report == '2':

		query_report = """SELECT
							EXTRACT(YEAR FROM w.time) || '-' ||to_char(w.time, 'MM')  as mes,
							sum(w.entry) as total_mes
							from statistics.dynamic w
							inner join statistics.camera on camera.id = w.camera_id
							inner join statistics.door on door.id = camera.door_id
							inner join statistics.mall on mall.id = door.mall_id
							where mall.active=True """+str(c)+""" """+str(d)+""" """+str(e)+"""
							group by
							EXTRACT(YEAR FROM w.time),
							to_char(w.time, 'MM')
							order by 1,2;
							"""
		q = connections['default'].cursor()
		q.execute(query_report)
		row = q.fetchall()

		for data in row:


			l = {'x':str(data[0]),
				'y':str(data[1]),
				}

			listx += [l]

	# Si selecciona entrada por accesos
	elif type_report == '3':

		query_report = """SELECT
							door.door_name,
							Sum(w.entry) AS total_mes
							from statistics.dynamic w
							inner join statistics.camera on camera.id = w.camera_id
							inner join statistics.door on door.id = camera.door_id
							inner join statistics.mall on mall.id = door.mall_id
							where mall.active=True """+str(c)+""" """+str(d)+""" """+str(e)+"""
							group by
							door.door_name
							order by 1,2;
							"""
		q = connections['default'].cursor()
		q.execute(query_report)
		row = q.fetchall()

		for data in row:


			l = {'x':str(data[0]),
				'y':str(data[1]),
				}

			listx += [l]

	# Si selecciona entarada por dias del mes
	elif type_report == '4':

		query_report = """SELECT trim(case when to_char(w.time,'d') = '1' then 'DOMINGO'
							when to_char(w.time,'d') = '2' then 'LUNES'
							when to_char(w.time,'d') = '3' then 'MARTES'
							when to_char(w.time,'d') = '4' then 'MIERCOLES'
							when to_char(w.time,'d') = '5' then 'JUEVES'
							when to_char(w.time,'d') = '6' then 'VIERNES'
							when to_char(w.time,'d') = '7' then 'SABADO'
							end) as dia,
							Sum(w.entry) AS total_dia
							from statistics.dynamic w
							inner join statistics.camera on camera.id = w.camera_id
							inner join statistics.door on door.id = camera.door_id
							inner join statistics.mall on mall.id = door.mall_id
							where mall.active=True """+str(c)+""" """+str(d)+""" """+str(e)+"""
							group by
							trim(case when to_char(w.time,'d') = '1' then 'DOMINGO'
							when to_char(w.time,'d') = '2' then 'LUNES'
							when to_char(w.time,'d') = '3' then 'MARTES'
							when to_char(w.time,'d') = '4' then 'MIERCOLES'
							when to_char(w.time,'d') = '5' then 'JUEVES'
							when to_char(w.time,'d') = '6' then 'VIERNES'
							when to_char(w.time,'d') = '7' then 'SABADO'
							end),to_char(w.time,'d')
							order by to_char(w.time,'d'),2;
							"""
		q = connections['default'].cursor()
		q.execute(query_report)
		row = q.fetchall()

		for data in row:


			l = {'x':str(data[0]),
				'y':str(data[1]),
				}

			listx += [l]

	# Si selecciona diario por horas
	elif type_report == '5':

		query_report = """SELECT to_timestamp(floor((extract('epoch' from w.time) / 3600 )) * 3600)
								AT TIME ZONE 'UTC' as timestamp,
								sum(w.entry)
								from statistics.dynamic w
								inner join statistics.camera on camera.id = w.camera_id
								inner join statistics.door on door.id = camera.door_id
								inner join statistics.mall on mall.id = door.mall_id
								where mall.active=True """+str(c)+""" """+str(d)+""" """+str(e)+"""
								group by
								timestamp
								order by 1,2;
								"""
		q = connections['default'].cursor()
		q.execute(query_report)
		row = q.fetchall()

		for data in row:


			l = {'x':str(data[0]),
				'y':str(data[1]),
				}

			listx += [l]


	return HttpResponse(json.dumps(listx))

def report_networking(request):

	#Se envia las variables para la ejecucion del reporte
	mall_id = request.POST['mall_cd']

	try:
		door_id = request.POST["door"]
	except KeyError:
		door_id = "todas"
	type_report = request.POST['type_report']
	date1 = request.POST['date1']
	date2 = request.POST['date2']
	listx = []


	# CONSOLIDADO
	if mall_id == "consolidado":

		#Hago una consulta de todos los centros comerciales activos para sacar el nombre
		query_mall = Mall.objects.filter(active=True,company_id=request.session['company_id']).order_by('id')

		x = ()

		for data in query_mall:

			i = (data.id,)

			x += (i)

		mall=x
	# Si selecciona solo un centro comercial
	else:
		mall= '('+request.POST['mall_cd']+')'
	# Si existe las variables se van creando los where
	if date1 and date2:
		date_start = date1+" 00:00:00"
		date_end = date2+" 23:59:59"

		c =  "and w.time BETWEEN '"+date_start + "' AND '" +date_end+"'"

	else:
		c = ""

	if door_id == "todas":
		d = ""
	else:

		d =  "and door.id in ("+ str(door_id)+")"


	if mall:

		e =  "AND mall.id in "+str(mall)
	else:
		e = ""
	# Si selecciona entradas por anio
	if type_report == '1':

		query_report = """SELECT
							date_part('year',w.time) as anio,
							sum(w.entry) as entrada
							from statistics.dynamic w
							inner join statistics.camera on camera.id = w.camera_id
							inner join statistics.door on door.id = camera.door_id
							inner join statistics.mall on mall.id = door.mall_id
							where mall.active=True """+str(c)+""" """+str(d)+""" """+str(e)+"""
							group by date_part('year',w.time)
							order by 1,2;
							"""
		q = connections['default'].cursor()
		q.execute(query_report)
		row = q.fetchall()

	# Si selecciona entradas por mes
	elif type_report == '2':

		query_report = """SELECT
							EXTRACT(YEAR FROM w.time) || '-' ||to_char(w.time, 'MM')  as mes,
							sum(w.entry) as total_mes
							from statistics.dynamic w
							inner join statistics.camera on camera.id = w.camera_id
							inner join statistics.door on door.id = camera.door_id
							inner join statistics.mall on mall.id = door.mall_id
							where mall.active=True """+str(c)+""" """+str(d)+""" """+str(e)+"""
							group by
							EXTRACT(YEAR FROM w.time),
							to_char(w.time, 'MM')
							order by 1,2;
							"""
		q = connections['default'].cursor()
		q.execute(query_report)
		row = q.fetchall()

	# Si selecciona entradas por accesos
	elif type_report == '3':

		query_report = """SELECT
							door.door_name,
							Sum(w.entry) AS total_mes
							from statistics.dynamic w
							inner join statistics.camera on camera.id = w.camera_id
							inner join statistics.door on door.id = camera.door_id
							inner join statistics.mall on mall.id = door.mall_id
							where mall.active=True """+str(c)+""" """+str(d)+""" """+str(e)+"""
							group by
							door.door_name
							order by 1,2;
							"""
		q = connections['default'].cursor()
		q.execute(query_report)
		row = q.fetchall()


	# Si selecciona entradas por dias del mes
	elif type_report == '4':

		query_report = """SELECT trim(case when to_char(w.time,'d') = '1' then 'DOMINGO'
							when to_char(w.time,'d') = '2' then 'LUNES'
							when to_char(w.time,'d') = '3' then 'MARTES'
							when to_char(w.time,'d') = '4' then 'MIERCOLES'
							when to_char(w.time,'d') = '5' then 'JUEVES'
							when to_char(w.time,'d') = '6' then 'VIERNES'
							when to_char(w.time,'d') = '7' then 'SABADO'
							end) as dia,
							Sum(w.entry) AS total_dia
							from statistics.dynamic w
							inner join statistics.camera on camera.id = w.camera_id
							inner join statistics.door on door.id = camera.door_id
							inner join statistics.mall on mall.id = door.mall_id
							where mall.active=True """+str(c)+""" """+str(d)+""" """+str(e)+"""
							group by
							trim(case when to_char(w.time,'d') = '1' then 'DOMINGO'
							when to_char(w.time,'d') = '2' then 'LUNES'
							when to_char(w.time,'d') = '3' then 'MARTES'
							when to_char(w.time,'d') = '4' then 'MIERCOLES'
							when to_char(w.time,'d') = '5' then 'JUEVES'
							when to_char(w.time,'d') = '6' then 'VIERNES'
							when to_char(w.time,'d') = '7' then 'SABADO'
							end),to_char(w.time,'d')
							order by to_char(w.time,'d'),2;
							"""
		q = connections['default'].cursor()
		q.execute(query_report)
		row = q.fetchall()


	# Si selecciona entradas diario por horas
	elif type_report == '5':

		query_report = """SELECT to_timestamp(floor((extract('epoch' from w.time) / 3600 )) * 3600)
								AT TIME ZONE 'UTC' as timestamp,
								sum(w.entry)
								from statistics.dynamic w
								inner join statistics.camera on camera.id = w.camera_id
								inner join statistics.door on door.id = camera.door_id
								inner join statistics.mall on mall.id = door.mall_id
								where mall.active=True """+str(c)+""" """+str(d)+""" """+str(e)+"""
								group by
								timestamp
								order by 1,2;
								"""
		q = connections['default'].cursor()
		q.execute(query_report)
		row = q.fetchall()


	file_path = 'grafic_report.xls'

	# Si existe data en el query ejecutado
	if row:

		response = HttpResponse(content_type="application/vnd.ms-excel")
		response['Content-Disposition'] = 'attachment; filename=' + os.path.basename(file_path)
		wb = xlwt.Workbook(encoding='utf-8')
		ws = wb.add_sheet("MyModel")

		row_num = 0

		columns = [
				(u"DATO", 6000),
		        (u"ENTRADA", 6000),

		    ]

		font_style = xlwt.XFStyle()
		font_style.font.bold = True

		for col_num in xrange(len(columns)):
		        ws.write(row_num, col_num, columns[col_num][0], font_style)
		        # set column width
		        ws.col(col_num).width = columns[col_num][1]

		font_style = xlwt.XFStyle()
		font_style.alignment.wrap = 1


		for dat in row:
			if type_report == '5':
				dato = validar_fecha_vacia(dat[0])
			else:
				dato = dat[0]

			row_num += 1
			row = [
	            dato,
	            dat[1],

	        ]
			for col_num in xrange(len(row)):
				ws.write(row_num, col_num, row[col_num], font_style)

		wb.save(response)
		return response

	else:
		messages.add_message(request, messages.INFO, 'No existe data para los filtro seleccionados')
		return redirect("/statistics/statistics/networking/")




def cameras(request):
	if not 'role_id' in request.session:
		return redirect('/')

	if request.session['role_id'] == 3 or request.session['role_id'] == 1:
		camera = Camera.objects.filter(active=True)
		if request.session['role_id'] == 1:
			mall = Mall.objects.filter(active=True,company_id=request.session['company_id']).order_by('id')


		elif request.session['role_id'] == 2 or request.session['role_id'] == 3:
			search_mall = UserDetail.objects.filter(user_id=request.session['user_id'])
			result_mall = search_mall[0].mall_id
			mall = Mall.objects.filter(active=True, id=result_mall).order_by('id')


		mall_lista =[]

		a = 0

		for dat in mall:

			cam = Camera.objects.filter(active=True, door__mall__id=dat.id)

			select_id = "camera_id_"+str(a)
			div_id = "div_id_"+str(a)
			cam_list = []
			for data in cam:
				x = {
				'id':data.id,
				'name':data.camera_name,
				}
				cam_list+=[x]

			i = {

				'mall' : dat.mall_name,
				'mall_id': dat.id,
				'select_id': select_id,
				'div_id': div_id,
				'cam':cam_list,
			}

			mall_lista+=[i]
			a = a+1

		return render(request, "statistics/camera.html", locals())
	else:
		raise PermissionDenied

def modal(request):
	camera_id = request.POST["camera_id"]
	mall_id = request.POST["mall_id"]
	camera_name = Camera.objects.filter(id=camera_id)
	if camera_name:
		result_camera_name = camera_name[0].camera_name
	return render(request, "statistics/modal.html", locals())



#Apagado de camara individual
def protocolo(request):
	c = 0
	try:
		x = int(request.POST["camera_id"])
		i = int(request.POST["mall_id"])

	except Exception as e:
		x = None
		i = None



	broker_address = ip()


	print("Publishing message to topic",ruta())
	data = {
                "sinapsis": "2.0",
                "camera_id": x, #cambiar por x
                "mall_id": i, #cambiar por i
                "request": {
                    "SystemOperation": {
                        "shutdown": True
        								}

     						}
			}


	try:
		publish.single(
			topic = ruta(),
			payload = json.dumps(data),
			hostname = ip(),
			qos = 2

		)

		c = 1
		messages.add_message(request, messages.INFO, 'Camara Apagada.')

	except Exception as e:
		print e
		messages.add_message(request, messages.ERROR, 'Ha ocurrido un error.')


	return HttpResponse(json.dumps(c))

#Apagado de camaras por lote
def protocolo_lote(request):

	cron = Cron.objects.filter(active=True)
	hour=current_time()


	for data in cron:

		if data.hour.current_hour == hour:

			mall_id = int(data.mall_id)
			#print mall_id

			camera = Camera.objects.filter(door__mall__id=mall_id,active=True)

			for dat in camera:
				broker_address = ip()
				camera_id = int(dat.id)

				print("Publishing message to topic",ruta())
				data = {
			                "sinapsis": "2.0",
			                "camera_id": camera_id, #cambiar por dat.id
			                "mall_id": mall_id, #cambiar por mall_id
			                "request": {
			                    "SystemOperation": {
			                        "shutdown": True
			        								}

			     						}
						}


				try:
					publish.single(
						topic = ruta(),
						payload = json.dumps(data),
						hostname = ip(),
						qos = 2

					)




				except Exception as e:
					print e




	return redirect("/statistics/statistics/")


#Funcion para encender el Relay
def activate(request):

	c = 0
	try:
		x = request.POST["camera_id"]
		i = request.POST["mall_id"]

		camera = Camera.objects.filter(id=x)
		ip_mall = camera[0].ip_camera
		channel = camera[0].channel

		list_main = ['name_archive', ip_mall,'close:'+str(channel)]

		main(list_main)
		c = 1
		messages.add_message(request, messages.INFO, 'Camara encendida.')
	except Exception as e:
		x = None
		i = None
		print e
		messages.add_message(request, messages.ERROR, 'Ha ocurrido un error.')


	return HttpResponse(json.dumps(c))

#Funcion para encender el Relay por lote
def activate_lote(request):
	cron = Cron.objects.filter(active=True)
	hour=current_time()


	for data in cron:

		if data.hour.current_hour == hour:

			mall_id = data.mall_id

			camera = Camera.objects.filter(door__mall__id=mall_id,active=True)

			for dat in camera:

				ip_mall = dat.ip_camera
				channel = dat.channel



				list_main = ['name_archive', ip_mall,'close:'+str(channel)]

				main(list_main)



	return redirect("/statistics/statistics/")


def cron(request):

	if not 'role_id' in request.session:
		return redirect('/')

	if request.session['role_id'] == 3 or request.session['role_id'] == 1:

		filtro = {'active':True}
		filtro2 = {}
		if request.session['role_id'] == 1:
				filtro2.update({'active':True,"company_id":request.session['company_id']})
		elif request.session['role_id'] == 3:
			search_mall = UserDetail.objects.filter(user_id=request.session['user_id'])
			result_mall = search_mall[0].mall_id
			filtro.update({"mall_id":result_mall})
			filtro2.update({'active':True,"id":result_mall})
		mall = Mall.objects.filter(**filtro2).order_by('id')
		cron = Cron.objects.filter(**filtro)
		listx = []

		for data in cron:



			l = {'mall':data.mall.mall_name,
				'cron_name':data.cron_name,
				'mall_id':data.mall_id,
				'type_cron':data.type_cron,
				'id':data.id,
				}

			listx += [l]

		return render(request, "statistics/cron.html", locals())
	else:
		raise PermissionDenied

@csrf_exempt
def filter_cron(request):
	filtro = {'active':True}
	if request.POST['mall_id'] != 'fvi':
		filtro.update({"mall_id":request.POST['mall_id']})


	cron = Cron.objects.filter(**filtro)


	listx = []
	for data in cron:



		l = {'mall':data.mall.mall_name,
			'cron_name':data.cron_name,
			'mall_id':data.mall_id,
			'type_cron':data.type_cron,
			'id':data.id,
			}

		listx += [l]

	return render(request, "statistics/cron_list.html", locals())

def new_cron(request):

	if not 'role_id' in request.session:
		return redirect('/')

	if request.session['role_id'] == 3 or request.session['role_id'] == 1:
		if not request.POST:
			filtro = {}
			if request.session['role_id'] == 1:
				filtro.update({'active':True,"company_id":request.session['company_id']})


			elif request.session['role_id'] == 3:
				search_mall = UserDetail.objects.filter(user_id=request.session['user_id'])
				result_mall = search_mall[0].mall_id
				filtro.update({'active':True,"id":result_mall})

			mall = Mall.objects.filter(**filtro).order_by('id')
			#extra(where=["mall.id not in(SELECT cron.mall_id FROM cron where active=True and type_cron=1)"])
			hour = Hour.objects.filter()
			return render(request, "statistics/new_cron.html", locals())
		else:

			hour = Hour.objects.filter(id=request.POST["hour_id"])
			x = hour[0].current_hour[3:5]
			y = hour[0].current_hour[:2]
			i = request.POST["mall_id"].strip()
			type_cron = request.POST["type_cron"]
			hour_id = int(request.POST["hour_id"])
			cron = Cron.objects.filter(mall_id=i,type_cron=type_cron,active=True)
			if not cron:

				cron_hour = Cron.objects.filter(active=True)
				for data in cron_hour:

					if data.hour_id == hour_id:

						messages.add_message(request, messages.ERROR, 'La Hora ya esta reservada para otro C.C Elija otra hora.')
						return redirect("/statistics/statistics/cron/")


				q= Cron(
				    mall_id = i,
				    create_time = timezone.now(),
				    create_id = request.session['user_id'],
				    active = True,
				    hour_id = hour_id,
				    type_cron = type_cron

				)
				q.save()

				cront_id = q.id


				my_cron = CronTab(user='root')
				if type_cron == '1' or type_cron == 1:
					job = my_cron.new(command='wget http://127.0.0.1:8000/statistics/statistics/activate_lote/', comment=str(cront_id))
				else:
					job = my_cron.new(command='wget http://127.0.0.1:8000/statistics/statistics/protocolo_lote/', comment=str(cront_id))

				job.minute.on(int(x))

				job.hour.on(int(y))


				for item in my_cron:
					print item
				Cron.objects.select_related().filter(id=cront_id).update(
													 cron_name = item)



				my_cron.write()
				messages.add_message(request, messages.INFO, 'Tarea Programada se ha creado.')
				return redirect("/statistics/statistics/cron/")
			else:
				messages.add_message(request, messages.ERROR, 'Tarea Programada ya existe para C.C y Tipo.')
				return redirect("/statistics/statistics/cron/")
	else:
		raise PermissionDenied

def cron_update(request,cron_id):

	if not 'role_id' in request.session:
		return redirect('/')

	if request.session['role_id'] == 3 or request.session['role_id'] == 1:
		cron = Cron.objects.filter(id=cron_id)
		if not request.POST:
			hour = Hour.objects.filter()
			cron_list = []
			for data in cron:


				i = {

					'cron_name' : data.cron_name,
					'mall_id' : data.mall_id,
					'hour' : data.hour_id,
					'type_cron': data.type_cron,
					'id': data.id,
				}
			filtro = {}
			if request.session['role_id'] == 1:
				filtro.update({'active':True,"company_id":request.session['company_id']})


			elif request.session['role_id'] == 3:
				search_mall = UserDetail.objects.filter(user_id=request.session['user_id'])
				result_mall = search_mall[0].mall_id
				filtro.update({'active':True,"id":result_mall})

			mall = Mall.objects.filter(**filtro).order_by('id')
			return render(request, "statistics/cron_update.html", locals())
		else:

			i = request.POST["id"].strip()
			hour_id = int(request.POST["hour_id"])
			hour = Hour.objects.filter(id=hour_id)
			x = hour[0].current_hour[3:5]
			y = hour[0].current_hour[:2]

			cron_hour = Cron.objects.filter(active=True)
			for data in cron_hour:

				if data.hour_id == hour_id:

					messages.add_message(request, messages.ERROR, 'La Hora ya esta reservada para otro C.C Elija otra hora.')
					return redirect("/statistics/statistics/cron/")

			my_cron = CronTab(user='root')
			for job in my_cron:
				print job
				if job.comment == str(i):

					job.minute.on(int(x))

					job.hour.on(int(y))
					cron.update(cron_name=job, hour_id = request.POST["hour_id"])
			my_cron.write()
			print 'Cron job modified successfully'


			messages.add_message(request, messages.INFO, 'La tarea ha sido editada.')
			return redirect("/statistics/statistics/cron/")
	else:
		raise PermissionDenied

def cron_delete(request,cron_id):
	cron = Cron.objects.filter(id=cron_id)

	my_cron = CronTab(user='root')
	for job in my_cron:
		if job.comment == str(cron_id):
			my_cron.remove(job)
			my_cron.write()

	cron.update(active=False)
	messages.add_message(request, messages.WARNING, 'La tarea ha sido eliminada.')
	return redirect("/statistics/statistics/cron/")


def maps(request):

	if not 'role_id' in request.session:
		return redirect('/')

	if not request.POST:
		if request.session['role_id'] == 1:
			mall = Mall.objects.filter(active=True).order_by('id')
		else:
			search_mall = UserDetail.objects.filter(user_id=request.session['user_id'])
			result = search_mall[0].mall_id
			mall = Mall.objects.filter(active=True, id=result).order_by('id')

		return render(request, "statistics/maps.html", locals())
	else:
		mall_id = request.POST['mall_id']
		mall_map = MallMaps.objects.filter(mall_id=mall_id)
		listx = []
		a = 0
		for data in mall_map:
			a = a + 1

			l = {'route':str(data.route),
				'map_name':str(data.map_name),
				'easyzoom_id':str("easyzoom_"+str(a)),
				}

			listx += [l]
		return HttpResponse(json.dumps(listx))
