from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from statistics.models import Company
from statistics.functions import ip
from index.models import User, Role, UserDetail
from django.views.decorators.csrf import csrf_exempt
try:
    from hashlib import sha1 as _sha, md5 as _md5
except ImportError:
    import sha
    import md5
    _sha = sha.new
    _md5 = md5.new

from datetime import datetime, timedelta, date
import time
import os
from time import gmtime, strftime
from django.contrib import messages


def index(request):

	if not request.POST:
		#print "HACIENDO PING A "+str(ip())
		#response = os.system("ping -c 1 "+ ip())
		#if response == 0:
		#	print "HOST CONECTADO"
		return render(request, "index/sign-in6.html")
		#else:
		#	print "HOST NO CONECTADO"
		#	return render(request, "offline.html")
	
		

	else:

		password_md5 = str(_md5(request.POST['password']).hexdigest())
		query_user = User.objects.filter(email=request.POST['email'], password = str(password_md5), active = True).values()
		if not query_user:
			messages.error(request,'Usuario o password incorrecto') 
			return render(request, "index/sign-in6.html")
			
		
		else:
			
			for data in query_user:

				request.session['user_id']		  	   = data['id']
				request.session['user']    	   		   = data['user_name']
				request.session['user_name']		   = (data['first_name']+" "+data['last_name']).title()
				request.session['email']       		   = data['email']
				request.session['role_id']             = data['role_id']
				request.session['cellphone_number']    = data['cellphone_number']
				request.session['hora']       		   = strftime("%H:%M", gmtime())
				request.session['complete'] = 1

				
				users = UserDetail.objects.filter(user_id=data['id']).values()
				

				request.session['company_id'] = users[0]['company_id']

				request.session.set_expiry(28800)

			if request.session['role_id'] == 1:
				
				request.session['mall_id'] = 0

			elif request.session['role_id'] == 2:

				request.session['mall_id'] = users[0]['mall_id']
			 	
				
			if request.session['role_id'] == 3:

				return redirect("/statistics/statistics/cameras/")

			else:
				return redirect("/statistics/statistics")



def destroy_session(request):
	request.session.delete()
	return redirect("/")

def handler403(request):
    return render(request, '403.html', status=403)

def handler404(request):
    return render(request, '404.html', status=404)

def handler500(request):
    return render(request, '500.html', status=500)
        
