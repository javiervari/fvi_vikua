
$(function() {

    var data = [{
        label: "El Tolon <b>413<b/>",
        data: 1,
        color:'#1D295B'
    }, {
        label: "Paseo El Hatillo <b>413<b/>",
        data: 3,
        color:'#E65525'
    }, {
        label: "Llano Mall <b>413<b/>",
        data: 9,
        color:'#ffb81d'
    }];

    var plotObj = $.plot($("#flot-pie-chart"), data, {
        series: {
            pie: {
                show: true
            }
        },
        grid: {
             color:'#ffffff',
            hoverable: true,
            borderWidth:1,
            borderColor: 'rgba(198, 198, 198, 0.8)'
        },
        tooltip: true,
        tooltipOpts: {
            content: "%p.0%, %s", // show percentages, rounding to 2 decimal places
            shifts: {
                x: 20,
                y: 0
            },
            defaultTheme: false
        },
        legend:{ 
            backgroundColor: 'rgba(0, 0, 0, 0.4)',
            color:'#ffffff'
        }
    });

});