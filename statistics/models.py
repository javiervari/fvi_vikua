# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Camera(models.Model):
    camera_name = models.CharField(max_length=50, blank=True, null=True)
    door = models.ForeignKey('Door', models.DO_NOTHING, blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    create = models.ForeignKey('index.User', models.DO_NOTHING, blank=True, null=True)
    active = models.NullBooleanField()
    ip_camera = models.CharField(max_length=50, blank=True, null=True)
    channel = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'camera'


class Company(models.Model):
    company_name = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=250, blank=True, null=True)
    phone = models.CharField(max_length=50, blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    create = models.ForeignKey('index.User', models.DO_NOTHING, blank=True, null=True)
    active = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'company'


class Door(models.Model):
    door_name = models.CharField(max_length=50, blank=True, null=True)
    mall = models.ForeignKey('Mall', models.DO_NOTHING, blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    create = models.ForeignKey('index.User', models.DO_NOTHING, blank=True, null=True)
    active = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'door'


class Dynamic(models.Model):
    camera = models.ForeignKey(Camera, models.DO_NOTHING, blank=True, null=True)
    time = models.DateTimeField(blank=True, null=True)
    entry = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    exit = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dynamic'


class EventLog(models.Model):
    user = models.ForeignKey('index.User', models.DO_NOTHING, blank=True, null=True)
    mall = models.ForeignKey('Mall', models.DO_NOTHING, blank=True, null=True)
    camera = models.ForeignKey(Camera, models.DO_NOTHING, blank=True, null=True)
    event_log_time = models.DateTimeField(blank=True, null=True)
    event_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'event_log'


class Mall(models.Model):
    mall_name = models.CharField(max_length=50, blank=True, null=True)
    company = models.ForeignKey(Company, models.DO_NOTHING, blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    create = models.ForeignKey('index.User', models.DO_NOTHING, blank=True, null=True)
    active = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'mall'

class Cron(models.Model):
    cron_name = models.CharField(max_length=250, blank=False , null=False)
    mall = models.ForeignKey('Mall', models.DO_NOTHING, blank=True, null=True)
    create = models.ForeignKey('index.User', models.DO_NOTHING, blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    active = models.NullBooleanField()
    hour = models.ForeignKey('Hour', models.DO_NOTHING, blank=True, null=True)
    type_cron = models.IntegerField(blank=True, null=True)
    class Meta:
        managed = False
        db_table = 'cron'


class Hour(models.Model):
    current_hour = models.CharField(max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'hour'


class MallMaps(models.Model):
    mall = models.ForeignKey('Mall', models.DO_NOTHING, blank=True, null=True, related_name='mall_id')
    route = models.CharField(max_length=50, blank=True, null=True)
    map_name = models.CharField(max_length=50, blank=True, null=True)
    active = models.NullBooleanField()


    class Meta:
        managed = False
        db_table = 'mall_maps'

# Vistas de postgres para consultas varias


class ViewCardsBefore(models.Model):
    mall = models.ForeignKey('Mall', models.DO_NOTHING, blank=True, null=True, related_name='mall_id_view_cards_before')    
    entry_before = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    exit_before = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'view_cards_before'

class ViewCardsNow(models.Model):
    mall = models.ForeignKey('Mall', models.DO_NOTHING, blank=True, null=True, related_name='mall_id_view_cards_now')    
    entry = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    exit = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'view_cards_now'


class ViewByHours(models.Model):
    time_block = models.DateTimeField(blank=True, null=True)
    mall = models.ForeignKey('Mall', models.DO_NOTHING, blank=True, null=True, related_name='mall_id_view')
    mall_name = models.CharField(max_length=50, blank=True, null=True)
    entry = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    exit = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'view_by_hours'


class ViewByDoors(models.Model):
    mall = models.ForeignKey('Mall', models.DO_NOTHING, blank=True, null=True, related_name='mall_id_view_doors')
    door_name = models.CharField(max_length=50, blank=True, null=True)
    entry = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    exit = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'view_by_doors'


class ViewDetail(models.Model):
    time = models.DateTimeField(blank=True, null=True)
    mall = models.ForeignKey('Mall', models.DO_NOTHING, blank=True, null=True, related_name='mall_id_details')
    door = models.ForeignKey('Door', models.DO_NOTHING, blank=True, null=True, related_name='door_id_details')
    sum_entry = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    sum_exit = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    inside = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'view_detail'