$( document ).ready(function() {

// FILTRO PARA GRAFICO DE TORTA

var mall_id;

$('#tolon').click(function () {
        ajax_pie(1);
    });
$('#hatillo').click(function () {
        ajax_pie(2);
    });
$('#llanomall').click(function () {
        ajax_pie(3);
    });

function ajax_pie(mall_id) {
	var parametros = {
	   "mall_id" : mall_id
	};
	$.ajax({
	   data:  parametros,
	   url:   '../statistics/load_chart_doors/',
	   type:  'post',
	   cache: false,
	    beforeSend: function(){
	                $('#flot-pie-chart').html("<div class='col-xs-16'><div class='load1 load-wrapper'><div class='loader'></div></div></div>");
	                $('#flot-pie-chart2').html("<div class='col-xs-16'><div class='load1 load-wrapper'><div class='loader'></div></div></div>");
	                },
	    success:  function (response) {
	    	console.log(JSON.parse(response));
	        var data = JSON.parse(response);
	        var data_entry = (data.data_entry);
	        var data_exit = (data.data_exit);
	        $.plot($("#flot-pie-chart"), data_entry, options);
	        $.plot($("#flot-pie-chart2"), data_exit, options);
	    },
	});
}

var dynamic;


// danger
$('#freq-button').click(function () {
    if (this.innerText == "Ver Entradas"){
    	ajax_line("entry");
    	$('#freq-button').empty();
    	$('#freq-button').text('Ver Salidas')
    	$('#freq-button').removeClass( "btn-outline-success" ).addClass( "btn-outline-info" );
    	console.log(this)
    }
    else if (this.innerText == "Ver Salidas"){
    	ajax_line("exit");
    	$('#freq-button').empty();
    	$('#freq-button').text('Ver Entradas')
    	$('#freq-button').removeClass( "btn-outline-info" ).addClass( "btn-outline-success" );
    	console.log(this)

    }
 });




// FILTRO PARA GRAFICO DE LINEA
function ajax_line(dynamic) {
	var parametros = {
	   "dynamic" : dynamic
	};
	$.ajax({
	   data:  parametros,
	   url:   '../statistics/load_chart_hours_blocks/',
	   type:  'post',
	   cache: false,
	    beforeSend: function(){
                $('#chartdiv').html("<div class='col-xs-16'><div class='load1 load-wrapper'><div class='loader'></div></div></div>");
                $("#freq-button").css("display", "none");
                },
    	success:  function (response) {
        chart.dataProvider = JSON.parse(response);
        if (parametros.dynamic == "entry"){
        	chart.valueAxes[0].title = "Entradas";
        }else{
        	chart.valueAxes[0].title = "Salidas";
        }
        chart.write("chartdiv");
        chart.validateData();
        $("#freq-button").css("display", "block");
    	},
	});
}

$('#mall_select').change(function(){
    var parametros = {
       "mall" : $("#mall_select").val(),
       "door": "todas"
       };
    $.ajax({
       data:  parametros,
       url:   '../statistics/filter_mall_detail_table/',
       type:  'post',
       cache: false,
        beforeSend: function(){
                $('#detail_table').html("<div class='col-xs-16'><div class='load1 load-wrapper'><div class='loader'></div></div></div>");
                $('#mall_select').css("display", "none");
                $('#door_select').css("display", "none");
                },
        success:  function (response) {
         var data = JSON.parse(response);
        var data_table = (data.table);
        var data_door = (data.door);
        var table = $('#detail_table');
        var select_door = $('#door_select');
        var select_mall = $('#mall_select');
        table.html("");
        select_door.html("");

        console.log(data_table);
        // ---- TABLA ----
        table.append("<thead><tr>");
        table.append("<th>Fecha</th><th>Hora</th><th>Entrada</th><th>Salida</th><th>Aforo</th>");
        table.append("</tr></thead>");
        $.each(data_table, function( index, value ) {
            table.append("<tbody><tr>");
            table.append("<td>"+value['date']+"</td>");
            table.append("<td>"+value['time']+"</td>");
            table.append("<td>"+value['sum_entry']+"</td>");
            table.append("<td>"+value['sum_exit']+"</td>");
            table.append("<td>"+value['sum_inside']+"</td>");
            table.append("</tr></tbody>");
        });

        // ---- TITULO ----
        $('#title_detail_table').empty();

        if ($("#mall_select").val() == "1"){
            $('#title_detail_table').text('EL TOLON');
        }else if ($("#mall_select").val() == "2"){
            $('#title_detail_table').text('EL HATILLO');
        }else if ($("#mall_select").val() == "3"){
            $('#title_detail_table').text('LLANO MALL');
        }else if ($("#mall_select").val() == "0"){
            $('#title_detail_table').text('CONSOLIDADO');
        };
        
        // ---- SELECT ----
        select_mall.css("display", "block");
        select_door.css("display", "block");
        //select_door.addClass('pull-right');
        select_door.append($('<option>', {
                value: 0,
                text: 'TODAS',
            }));
        $.each(data_door, function ( index, value ) {
            select_door.append($('<option>', {
                value: value.door_id,
                text: value.door_name,
            }));
        });
    },
   });
});


$('#door_select').change(function(){
    var parametros = {
       "mall" : $("#mall_select").val(),
       "door": $("#door_select").val()
       };
    $.ajax({
       data:  parametros,
       url:   '../statistics/filter_door_detail_table/',
       type:  'post',
       cache: false,
        beforeSend: function(){
                $('#detail_table').html("<div class='col-xs-16'><div class='load1 load-wrapper'><div class='loader'></div></div></div>");
                $('#mall_select').css("display", "none");
                $('#door_select').css("display", "none");
                },
        success:  function (response) {
        var data = JSON.parse(response);
        var table = $('#detail_table');
        var select_door = $('#door_select');
        var select_mall = $('#mall_select')
        table.html("");

        console.log(data);
        // ---- TABLA ----
        table.append("<thead><tr>");
        table.append("<th>Fecha</th><th>Hora</th><th>Entrada</th><th>Salida</th>");
        table.append("</tr></thead>");
        $.each(data, function( index, value ) {
            table.append("<tbody><tr>");
            table.append("<td>"+value['date']+"</td>");
            table.append("<td>"+value['time']+"</td>");
            table.append("<td>"+value['sum_entry']+"</td>");
            table.append("<td>"+value['sum_exit']+"</td>");
            table.append("</tr></tbody>");
        });

        // ---- TITULO ----
        $('#title_detail_table').empty();

        if ($("#mall_select").val() == "1"){
            $('#title_detail_table').text('EL TOLON');
        }else if ($("#mall_select").val() == "2"){
            $('#title_detail_table').text('EL HATILLO');
        }else if ($("#mall_select").val() == "3"){
            $('#title_detail_table').text('LLANO MALL');
        }else if ($("#mall_select").val() == "0"){
            $('#title_detail_table').text('CONSOLIDADO');
        };
        
        // ---- SELECT ----
        select_mall.css("display", "block");
        select_door.css("display", "block");
        //select_door.addClass('pull-right');
        // select_door.append($('<option>', {
        //         value: 0,
        //         text: 'TODAS',
        //     }));
        // $.each(data_door, function ( index, value ) {
        //     select_door.append($('<option>', {
        //         value: value.door_id,
        //         text: value.door_name,
        //     }));
        // });
    },
   });
});



});


