# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2018-05-28 17:52
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Camera',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('camera_name', models.CharField(blank=True, max_length=50, null=True)),
                ('create_time', models.DateTimeField(blank=True, null=True)),
                ('active', models.NullBooleanField()),
            ],
            options={
                'db_table': 'camera',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company_name', models.CharField(blank=True, max_length=100, null=True)),
                ('address', models.CharField(blank=True, max_length=250, null=True)),
                ('phone', models.CharField(blank=True, max_length=50, null=True)),
                ('create_time', models.DateTimeField(blank=True, null=True)),
                ('active', models.NullBooleanField()),
            ],
            options={
                'db_table': 'company',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Door',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('door_name', models.CharField(blank=True, max_length=50, null=True)),
                ('create_time', models.DateTimeField(blank=True, null=True)),
                ('active', models.NullBooleanField()),
            ],
            options={
                'db_table': 'door',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Dynamic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.DateTimeField(blank=True, null=True)),
                ('entry', models.DecimalField(blank=True, decimal_places=65535, max_digits=65535, null=True)),
                ('exit', models.DecimalField(blank=True, decimal_places=65535, max_digits=65535, null=True)),
            ],
            options={
                'db_table': 'dynamic',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='EventLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('event_log_time', models.DateTimeField(blank=True, null=True)),
                ('event', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'db_table': 'event_log',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Mall',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mall_name', models.CharField(blank=True, max_length=50, null=True)),
                ('create_time', models.DateTimeField(blank=True, null=True)),
                ('active', models.NullBooleanField()),
            ],
            options={
                'db_table': 'mall',
                'managed': False,
            },
        ),
    ]
