import psycopg2, psycopg2.extras
import paho.mqtt.client
import ssl
import sys
import json 
import threading
import time
import os
from cryptography.fernet import Fernet
from datetime import datetime
from dateutil import tz

conn = psycopg2.connect(database='fvi',user='postgres',password='postgres', host='localhost')
           
def on_connect(client, userdata, flags, rc):
    
    print('connected (%s)' % client._client_id)
    client.subscribe(topic='iot/fvi/device', qos = 0)        

def on_message(client, userdata, message):
   
    cipher_key = b"UOC-DMXptS_t7JPi_omDofSfTYaYVTqn638NLMl_zYE="
    cipher = Fernet(cipher_key)
    
    msg = json.loads(message.payload.decode())
    data = msg.get("request", "0")
    

    if data == {'SystemOperation': {'shutdown': 'received'}}:
        
        print ("mensaje de apagado")

        time.sleep(20)
        x = msg['camera_id']
        print (x)
        cur = conn.cursor()
        cur.execute("""SELECT * FROM statistics.camera where camera.id ='"""+str(x)+"""'""")
        rows=cur.fetchall()
        #print rows

        ip_mall = rows[0][6]
        channel = rows[0][7]

        list_main = ['name_archive', ip_mall,'open:'+str(channel)]

        main(list_main)

    elif "Counting" in data :

        print ("Json conteo")
        camera_id = str(msg['camera_id'])
        # Convertir fecha en zona horaria Caracas
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('America/Caracas')
        time_request = str(msg['date']).replace('/','-')
        utc = datetime.strptime(time_request, '%Y-%m-%d %H:%M:%S')
        utc = utc.replace(tzinfo=from_zone)
        central = utc.astimezone(to_zone)
        time_data = datetime.strftime(central, '%Y-%m-%d %H:%M:%S')
        
        entry   = 0
        exit    = 0
        
        if 'in' in msg['request']['Counting']:
            entry = abs(int(msg['request']['Counting']['in']))
            
        if 'out' in msg['request']['Counting']:
            exit = abs(int(msg['request']['Counting']['out']))
            
        if entry == 0 and exit == 0:
            print ("Sin data")
        else:
            print ("camera: ", camera_id, "time: ", time_data, "entry: ", entry, "exit: ", exit)

            _json = {
                "camera_id": camera_id,
                "time_data": time_data,
                "entry": entry,
                "exit": exit,
            }
            print ("[JSON_onMessage] : ",_json)

            query = """
                        INSERT INTO statistics.dynamic(camera_id, "time", entry, exit) 
                        VALUES ( '"""+camera_id+"""', '"""+time_data+"""' ,'"""+str(entry)+"""','"""+str(exit)+"""' )
                    """
            cur = conn.cursor()
            cur.execute(query)
            conn.commit()
            cur.close() 
            #conn.close()

    else:
        print ("No esta contemplado")
    
client = paho.mqtt.client.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(host='104.236.228.139') 
client.loop_forever()

