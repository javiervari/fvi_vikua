from __future__ import unicode_literals

from django.db import models

class Role(models.Model):
    id = models.IntegerField(primary_key=True)
    role_name = models.CharField(max_length=50, blank=True, null=True)
    active = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'role'


class User(models.Model):
    user_name = models.CharField(max_length=100, blank=True, null=True, unique=True)
    password = models.TextField(max_length=100, blank=True, null=True)
    email = models.EmailField(max_length=50, blank=True, null=True, unique=True)
    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=30, blank=False)
    cellphone_number = models.CharField(max_length=20, blank=False)
    role = models.ForeignKey(Role, models.DO_NOTHING, blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    create_id = models.IntegerField(blank=True, null=True)
    active = models.BooleanField()

    USERNAME_FIELD = 'user_name'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = [
        'first_name',
        'last_name',
        'email',
        'role'
    ]


    class Meta:
        managed = False
        db_table = 'user'
        unique_together = ('email','user_name')

class UserDetail(models.Model): 
    user = models.ForeignKey('User', models.DO_NOTHING, blank=True, null=True, related_name="user")
    company = models.ForeignKey('statistics.Company', models.DO_NOTHING, blank=True, null=True)
    mall = models.ForeignKey('statistics.Mall', models.DO_NOTHING, blank=True, null=True)
    create_time = models.DateTimeField(blank=True, null=True)
    create = models.ForeignKey('index.User', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_detail'
        