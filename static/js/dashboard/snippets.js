//Funciones comunes y de uso general


//Spincrement
$(document).ready(function(){
  $('.spincrement').spincrement({
    from: 0,
    decimalPlaces: 0,
    thousandSeparator: false,
    duration: 2500, // ms; TOTAL length animation
    leeway: 50, // percent of duraion
    easing: 'spincrementEasing',
    fade: true
  });
});