//metodo para validar
$('document').ready(function(){ 
 
  //Muestro el Select correspondiente al Rol 
  "use strict";
  $('#mall_id_multi').multiselect();
  $('#mall_id_single').multiselect();

  if ($("#role_id").val()==1){
    $('#select_single').hide().attr('readonly', true);
  }else{
    $('#select_multiple').hide().attr('readonly', true);
  };

  $("#role_id").change(function() {
    if ($("#role_id").val()==1){
      $('#select_single').hide();
      $('#select_multiple').show();
    }else{
      $('#select_multiple').hide();
      $('#select_single').show();
    };


  });


   // name validation
   var nameregex = /^[a-zA-Z ]+$/;
   
   $.validator.addMethod("validname", function( value, element ) {
     return this.optional( element ) || nameregex.test( value );
   }); 
   
   // valid email pattern
  
   $.validator.addMethod("validemail", function( value, element ) {
     return this.optional( element ) || /[a-z]+@[a-z]+\.[a-z]+/.test( value );
   });

   
   $("#register-form").validate({

    rules:
    {
      first_name: {
       required: true,
       validname: true,
       minlength: 3
     },
      last_name: {
       required: true,
       validname: true,
       minlength: 3
     },
     email: {
       required: true,
       validemail: true
     },
     cellphone:{
      required: true
     },
     password: {
       minlength: 8,
       maxlength: 15
     },
     cpassword: {
       equalTo: '#password'
     },
     mall_id:{
      required: true
     },
     role_id:{
      required: true
     },
   },
   messages:
   {
      first_name: {
       required: "Por favor ingrese el nombre",
       validname: "El nombre debe contener solo alfabetos y espacio",
       minlength: "Tu nombre es muy corto"
     },
      last_name: {
       required: "Por favor ingrese el apellido",
       validname: "El apellido debe contener solo alfabetos y espacio",
       minlength: "Tu nombre es muy corto"
     },
      email: {
       required: "Por favor ingrese un correo",
       validemail: "Ingresa un correo valido"
     },
      cellphone:{
        required: "Por favor ingrese un numero de Telefono",
       },
      password:{
       required: "Por favor ingrese la contraseña",
       minlength: "La contraseña debe tener al menos 8 caracteres"
     },
      cpassword:{
       required: "Por favor repita la contraseña",
       equalTo: "Contraseña no coincide !"
     },
      mall_id:{
        required: "Por favor al menos 1 centro comercial",
       },
      role_id:{
        required: "Por favor seleccione un tipo de usuario",
     }
 },
 errorPlacement : function(error, element) {
   $(element).closest('.form-group').find('.help-block').html(error.html());
 },
 highlight : function(element) {
   $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
 },
 unhighlight: function(element, errorClass, validClass) {
   $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
   $(element).closest('.form-group').find('.help-block').html('');
 },

 submitHandler: function(form) {
  form.submit();
  //alert('ok');
}
}); 
 })


//Prevengo el submit, elimino el select que no corresponde al rol y luego lo envio
$("#register-form").submit(function(e){
  e.preventDefault();
  if ($("#role_id").val()==1){
    $('#select_single').remove()
  }else{
    $('#select_multiple').remove()
  };

  this.submit();

});