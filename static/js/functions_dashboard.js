$(document).ready(function(){

// Funciones multiselect

$('#date').multiselect({
    buttonWidth: '100%' 
});
$('#mall').multiselect({
    buttonWidth: '100%' 
});
$('#mall_historico').multiselect({
    buttonWidth: '100%' 
});
$('#dynamic').multiselect({
    buttonWidth: '100%' 
});
$('#month').multiselect({
    buttonWidth: '100%' 
});
$('#door_cd').multiselect({
    buttonWidth: '120%' 
});
$('#mall_cd').multiselect({
    buttonWidth: '100%' 
});

// #################### GRAFICO Y CUADRO ENTRADA/SALIDA ######################

//Pregunto si no viene nada de la vista le paso todos los parametros en 0
var dataEE  =  [ 
{% if not var_date %}
{ y: '10am/12pm', a: 0, b: 0 },
{ y: '12pm/2pm', a: 0,  b: 0 },
{ y: '2pm/4pm', a: 0,  b: 0 },
{ y: '4pm/6pm', a: 0,  b: 0 },
{ y: '6pm/8pm', a: 0,  b: 0 },
{ y: '8pm/10pm', a: 0,  b: 0 },
{ y: '10pm/12am', a: 0, b: 0 }
// Si viene data construyo la lista de los parametros a graficar
{% else %}
{% for i in var_date %}
{ y: '{{i.y}}', a: {{i.a}}, b: {{i.b}} },
{% endfor %}   
{% endif %}
];
// Configuración del resto de los parametros para el chart
var chartEE = {
    element: 'x1chart',
    data: dataEE,
    resize: true,
    xkey: 'y',
    ykeys: ['a', 'b'],
    labels: ['Entradas', 'Salidas'],
    barColors: ["#03A9F4", "#8BC34A", "#FFC107", "#F44336"]
};
//Ejecuto el constructor y lo meto en una variable para poder utilizarla en los ajax
var xchart = Morris.Bar(chartEE);

//Función Ajax para el filtro del Centro Comercial
$("#mall").change(function(){
    $("#date").html();
    var parametros = {
        "mall_id" : $("#mall").val(),
        //Mostrara siempre el valor diario inicialmente en el grafico
        "date": "1",
        "csrfmiddlewaretoken": '{{ csrf_token }}'
        };
    //Ajax para mostrar los filtros en el cuadro
    $.ajax({
        data:  parametros,
        url:   '../statistics/filters/',
        type:  'post',
        success:  function (response) {
            var date = $("#date");
            //Muestro la respuesta en el cuadro
            $("#show_list").html(response);
            //Aqui clavo el select del filtro de rango de tiempo a diario
            date.multiselect('deselect', date.val());
            date.multiselect('select','1');
        }
    });
    //Ajax para mostrar los filtros en el grafico bar chart
    $.ajax({
        data:  parametros,
        url:   '../statistics/filters_chart/',
        type:  'post',
        success:  function (response) {
            //Recibo el json que viene de la vista con el diccionario que contiene los datos del grafico
            var xo = JSON.parse(response);
            // Llamo a la variable xchart que construye el grafico y le seteo los datos del json
            xchart.setData(xo);
        }
    });
    //Ajax para modificar el titulo del grafico segun el centro comercial que seleccione
    $.ajax({
        data:  parametros,
        url:   '../statistics/filter_title/',
        type:  'post',
        success:  function (response) {
            var title = JSON.parse(response);
            //Modifico el label con la respuesta que viene de la lista
            $("#title_bar_char").html(title);
        }
    });
});

//Función Ajax para el filtro del rango de tiempo
$("#date").change(function(){
    var parametros = {
        "mall_id" : $("#mall").val(),
        "date" : $("#date").val(),
        "csrfmiddlewaretoken": '{{ csrf_token }}'
    };

    //Ajax para mostrar los filtros en el cuadro
    $.ajax({
        data:  parametros,
        url:   '../statistics/filters/',
        type:  'post',
        success:  function (response) {
            //Muestro la respuesta en el cuadro
            $("#show_list").html(response);
        }
    });

    //Ajax para mostrar los filtros en el grafico bar chart
    $.ajax({
        data:  parametros,
        url:   '../statistics/filters_chart/',
        type:  'post',
        success:  function (response) {
            //Recibo el json que viene de la vista con el diccionario que contiene los datos del grafico
            var xo = JSON.parse(response);
            // Llamo a la variable xchart que construye el grafico y le seteo los datos del json
            xchart.setData(xo);
        }
    });
});

// #################### GRAFICO Y CUADRO HISTORICO ######################

var yearx = '{% now "Y" %}';
var year = yearx.toString();
//Pregunto si no viene nada de la vista le paso todos los parametros en 0
var dataEE_line  = [  
{% if not var_date %}
{ y: '{% now "Y" %}'+"-01", a: 0, b: 0 },
{ y: '{% now "Y" %}'+"-02", a: 0,  b: 0 },
{ y: '{% now "Y" %}'+"-03", a: 0,  b: 0 },
{ y: '{% now "Y" %}'+"-04", a: 0,  b: 0 },
{ y: '{% now "Y" %}'+"-05", a: 0,  b: 0 },
{ y: '{% now "Y" %}'+"-06", a: 0,  b: 0 },
{ y: '{% now "Y" %}'+"-07", a: 0,  b: 0 },
// Si viene data construyo la lista de los parametros a graficar
{% else %}
{% for i in var_date_historico %}
{ y: '{{i.y}}', a: {{i.a}}, b: {{i.b}} },
{% endfor %}
{% endif %}
];
// Configuración del resto de los parametros para el chart
var chartEE_line = {
element: 'x2chart',
data: dataEE_line,
resize: true,
xkey: 'y',
ykeys: ['a', 'b'],
labels: ['Entradas', 'Salidas'],
lineColors: [ "#FFC107", "#F44336"],
};

//Ejecuto el constructor y lo meto en una variable para poder utilizarla en los ajax
var xchart = Morris.Area(chartEE_line);

//Función Ajax para el filtro del Centro Comercial
$("#mall_historico").change(function(){
    var current_year = '{% now "Y" %}';
    var parametros = {
        "mall_id" : $("#mall_historico").val(),
        "dynamic" : '1',
        "date" : '00',
        "csrfmiddlewaretoken": '{{ csrf_token }}'
    };
    //Ajax para mostrar los filtros en el cuadro
    $.ajax({
        data:  parametros,
        url:   '../statistics/filters_historico/',
        type:  'post',
        success:  function (response) {
            var dynamic = $("#dynamic");
            //Muestro la respuesta en el cuadro
            $("#show_list_historico").html(response);
            //Aqui clavo el select del filtro de entrada y salida en entrada;
            dynamic.multiselect('deselect', dynamic.val());
            dynamic.multiselect('select','1');
        }
    });
});

//Función Ajax para el filtro de entradas o salidas
$("#dynamic").change(function(){
    var current_month = '{% now "m" %}';
    var parametros = {
        "mall_id" : $("#mall_historico").val(),
        "dynamic" : $("#dynamic").val(),
        "date" : '00',
        "csrfmiddlewaretoken": '{{ csrf_token }}'
    };
    //Ajax para mostrar los filtros en el cuadro
    $.ajax({
        data:  parametros,
        url:   '../statistics/filters_historico/',
        type:  'post',
        success:  function (response) {
            var month = $("#month");
            var current_month = '{% now "m" %}';
            //Muestro la respuesta en el cuadro
            $("#show_list_historico").html(response);
            //Aqui clavo el select del filtro de mes en el mes actual
            month.multiselect('deselect', month.val());
            month.multiselect('select', current_month);
        }
    });
});

//Función Ajax para el filtro del mes 
$("#month").change(function(){
    var parametros = {
        "mall_id" : $("#mall_historico").val(),
        "dynamic" : $("#dynamic").val(),
        "date" : $("#month").val(),
        "csrfmiddlewaretoken": '{{ csrf_token }}'
    };
    //Ajax para mostrar los filtros en el cuadro
    $.ajax({
        data:  parametros,
        url:   '../statistics/filters_historico/',
        type:  'post',
        success:  function (response) {
            //Muestro la respuesta en el cuadro
            $("#show_list_historico").html(response);
        }
        });
});

// #################### CUADRO FINAL DETALLE ######################

//Función Ajax para el filtro por centro comercial
$("#mall_cd").change(function(){
    var parametros = {
    "mall_id" : $("#mall_cd").val(),
    //Mostrara siempre el valor de todos los accesos del centro comercial
    "door": "todas",
    "csrfmiddlewaretoken": '{{ csrf_token }}'
    };
    //Mostrar u ocultar filtro de acceso segun el centro comercial que seleccione
    //Si es consolidado se oculta, si es un centro comercial se muestra
    mall_id = $("#mall_cd").val();

    if (mall_id === "consolidado")
    {
        $("#show_door").css('display', 'none');
        $("#show_door").prop("value","");
    }else{
        $("#show_door").css('display', 'block');
    }
    //Ajax para mostrar los filtros en el cuadro
    $.ajax({
        data:  parametros,
        url:   '../statistics/filter_cuadro_detalle/',
        type:  'post',
        success:  function (response) {
            console.log(response);
            var data = JSON.parse(response);
            var tablax = $('#cuadro_detalle');
            tablax.html('');
            $.each(data, function( index, value ) {
                tablax.append('<tr class="odd">');
                tablax.append('<td>'+value['date']+'</td>');
                tablax.append('<td>'+value['hour']+'</td>');
                tablax.append('<td>'+value['entry']+'</td>');
                tablax.append('<td>'+value['exit']+'</td>');
                tablax.append('<td>'+value['in_mall']+'</td>');
                tablax.append('</tr>');
        });
        }
    });
    // Ajax para mostrar el onchage del acceso mediante el multiselect 
    $.ajax({
        data:  parametros,
        url:   '../statistics/filter_door/',
        type:  'post',
        success:  function (response) {
            console.log(response);
            var data_select = JSON.parse(response);
            $("#show_door").html('<select id="door"></select>');
            $('#door').multiselect({
                buttonWidth: '100%',
                // He aqui EL FUME
                onChange: function(option, checked, select) {
                    var parametros = {
                        "mall_id" : $("#mall_cd").val(),
                        "door": $("#door").val(),
                        "csrfmiddlewaretoken": '{{ csrf_token }}'
                    };
                    $.ajax({
                        data:  parametros,
                        url:   '../statistics/filter_cuadro_detalle/',
                        type:  'post',
                        success:  function (response) {
                            var data = JSON.parse(response);
                            $('#cuadro_detalle').html('');
                            $.each(data, function( index, value ) {
                            var date = value['date'];
                            var hour = value['hour'];
                            var entry = value['entry'];
                            var exit = value['exit'];
                            var in_mall = value['in_mall'];

                            $("#cuadro_detalle").append("<tr class='odd'>");
                            $("#cuadro_detalle").append("<td>"+date+"</td>");
                            $("#cuadro_detalle").append("<td>"+hour+"</td>");
                            $("#cuadro_detalle").append("<td>"+entry+"</td>");
                            $("#cuadro_detalle").append("<td>"+exit+"</td>");
                            $("#cuadro_detalle").append("<td>"+in_mall+"</td>");
                            $('#cuadro_detalle').append('</tr>');
                            });
                        }
                    });
                }
                });
            // Aqui reconstruyo el multiselect de accesos
            options = [];
            hash = {
                'value':'todas','title': 'Acceso', 'label': 'Todos los accesos'
            };
            options.push(hash);
            $.each(data_select, function( index, value ) {
                var valx = value['door_id'];
                var text = value['door_name'];
                hash = {
                    'value':valx,'title': text, 'label': text
                };
                options.push(hash);                            
            });
            $('#door').multiselect('dataprovider', options);
        }
    });
});

$("#door_cd").change(function(){
    var parametros = {
        "mall_id" : $("#mall_cd").val(),
        "door": $("#door_cd").val(),
        "csrfmiddlewaretoken": '{{ csrf_token }}'
    };
    $.ajax({
        data:  parametros,
        url:   '../statistics/filter_cuadro_detalle/',
        type:  'post',
        success:  function (response) {
            //alert('llega');
            var data = JSON.parse(response);
            $('#cuadro_detalle').html('');
            $.each(data, function( index, value ) {
            //alert(value['date']+value['hour']+value['entry']+value['exit']+value['in_mall']);
            var date = value['date'];
            var hour = value['hour'];
            var entry = value['entry'];
            var exit = value['exit'];
            var in_mall = value['in_mall'];

            $("#cuadro_detalle").append("<tr class='odd'>");
            $("#cuadro_detalle").append("<td>"+date+"</td>");
            $("#cuadro_detalle").append("<td>"+hour+"</td>");
            $("#cuadro_detalle").append("<td>"+entry+"</td>");
            $("#cuadro_detalle").append("<td>"+exit+"</td>");
            $("#cuadro_detalle").append("<td>"+in_mall+"</td>");
            $('#cuadro_detalle').append('</tr>');
        });
    }
    });
});

});
