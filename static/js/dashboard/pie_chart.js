
var options = {
        series: {
            pie: {
                show: true,
                fillAlphas: 0.6,
                stroke: {
                width: 0.1
                }
            }
        },
        grid: {
            color:'#ffffff',
            hoverable: true,
            borderWidth:1,
            borderColor: 'rgba(198, 198, 198, 0.8)'
        },
        tooltip: true,
        tooltipOpts: {
            content: "<div style='color:#000000'> %p.0%, %s </div>", // show percentages, rounding to 2 decimal places
            shifts: {
                x: -150,
                y: 20
            },

            defaultTheme: false
        },
        legend:{
            backgroundColor: "rgba(32, 44, 38)",
            color:"#ffffff",
        },
};

$.ajax({
    url:   '../statistics/load_chart_doors/',
    cache: false,
    beforeSend: function(){
                $('#flot-pie-chart').html("<div class='col-xs-16'><div class='load1 load-wrapper'><div class='loader'></div></div></div>");
                $('#flot-pie-chart2').html("<div class='col-xs-16'><div class='load1 load-wrapper'><div class='loader'></div></div></div>");
                },
    success:  function (response) {
        var data = JSON.parse(response);
        var data_entry = (data.data_entry);
        var data_exit = (data.data_exit);
        $.plot($("#flot-pie-chart"), data_entry, options);
        $.plot($("#flot-pie-chart2"), data_exit, options);
    },
});
