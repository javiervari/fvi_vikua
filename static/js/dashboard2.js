/*!
 * Adminux (http://maxartkiller.com)
 * Copyright 2017 The Adminux Author: Maxartkiller
 * purchase licence before use
 * You can not resale or modify without prior licences.
*/

/* Dashboard chart combo line and bar */

function addData(chart, label, data) {

    label.forEach((y) => {
        chart.data.labels.push(y);
    });
    
    chart.data.datasets.forEach((dataset) => {
        data.forEach((x) => {
            dataset.data.push(x);
        });

        
    });



    chart.update();
}

function removeData(chart) {
    var A = chart.data.labels;
    while(A.length > 0) {
        A.pop();
    }
    chart.data.datasets.forEach((dataset) => {
        while(dataset.data.length > 0) {
            dataset.data.pop();
        }        
    });
    chart.update();
}


"use strict";


$('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 240,
    itemMargin: 26,
    controlNav: false
});


/* prgress cricle */
$('.progress-success').circleProgress({ 
    fill: {gradient: ["#2dc1c9", "#0d769f   "]},
    lineCap: 'butt'
}).on('circle-animation-progress', function(event, progress,stepValue) {
    $(this).find('strong').html(Math.round(100 * progress * stepValue) + '<i>%</i>');
  });
$('.progress-danger').circleProgress({
      fill: {gradient:["#f6775a", "#ed5a7c"]},
}).on('circle-animation-progress', function(event, progress,stepValue) {
    $(this).find('strong').html(Math.round(100 * progress * stepValue) + '<i>%</i>');
  });
$('.progress-warning').circleProgress({ 
    fill: {gradient: ["#ff9300", "#ff5800"]},
    lineCap: 'butt'
}).on('circle-animation-progress', function(event, progress,stepValue) {
    $(this).find('strong').html(Math.round(100 * progress * stepValue) + '<i>%</i>');
  });
$('.progress-primary').circleProgress({ 
    fill: {gradient: ["#a758f5", "#7a79fe"]},
    lineCap: 'butt'
}).on('circle-animation-progress', function(event, progress,stepValue) {
    $(this).find('strong').html(Math.round(100 * progress * stepValue) + '<i>%</i>');
  });

 $('.spincreament').spincrement({
        from: 0,
        decimalPlaces: 0,
        thousandSeparator: false,
        duration: 1500, // ms; TOTAL length animation
        leeway: 50, // percent of duraion
        easing: 'spincrementEasing',
        fade: true

});