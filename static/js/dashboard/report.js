
$(document).ready(function(){

// Calendario
$(function($){
    $.datepicker.regional['es'] = {
      closeText: 'Cerrar',
      prevText: '<Ant',
      nextText: 'Sig>',
      currentText: 'Hoy',
      monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
      dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
      weekHeader: 'Sm',
      dateFormat: 'yy-mm-dd',
      firstDay: 1,
      isRTL: false,
      changeMonth: true,
      changeYear: true,
      showMonthAfterYear: false,
      yearSuffix: '',
      showAnim: "slideDown"
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
  });

  $('#date1').datepicker({
  });

  $('#date2').datepicker({
  });


//Multiselect
$('#type_report').multiselect({
    buttonWidth: '170%' 
  });
  $('#mall_cd').multiselect({
    buttonWidth: '170%' 
  });
  $('#door').multiselect({
    buttonWidth: '170%' 
  });


//funcion reset

  $('#mall_cd').multiselect();
  $('#agregar').on('reset', function() {
    $('#mall_cd option:selected').each(function() {
      $(this).prop('selected', false);
    })

    $('#mall_cd').multiselect('refresh');
  });

  
//GRAFICO
var ctx = document.getElementById("linechart").getContext('2d');
var gradient2 = ctx.createLinearGradient(0, 0, 0, 450);
gradient2.addColorStop(0, 'rgba(13,208,229,1)');   
gradient2.addColorStop(0.5, 'rgba(141,131,253,0.2)');

var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [],
    datasets: [{
      label: ' Entradas',
      backgroundColor: gradient2,
      data:[],
      borderColor: "rgba(13,208,229,1)",
      borderCapStyle: 'butt',
      borderDash: [],            
      borderDashOffset:1,
      borderJoinStyle: 'bevel',
      pointBorderColor: "#cccccc",
      pointBackgroundColor: "#cccccc",
      pointBorderWidth: 2,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      pointRadius: 3,
      pointHitRadius: 10,
    }]
  },
  options: {
    responsive:true,
    maintainAspectRatio: true,              
    title: {
      display: false,
      text: 'Chart.js  Line Chart',
    },
    legend: {
      display: false,
      labels:{ 
        fontColor:"#b7c8ff"}
      },
      scales: {
        yAxes: [{
          ticks: {
            fontColor: "#b7c8ff",
            beginAtZero: false,
          },
          gridLines:{
            color:"rgba(160,160,160,0.1",
            zeroLineColor:"rgba(160,160,160,0.1)"
          }
        }],
        xAxes: [{
          ticks: {
            fontColor: "#b7c8ff"
          },
          gridLines:{
            color:"rgba(160,160,160,0.1)",
            zeroLineColor:"rgba(160,160,160,0.1)"
          }
        }]
      }
    }
  });
    //hasta aqui

  //funcion para buscar las puertas de cada centro comercial
  $("#mall_cd").change(function(){
    var parametros = {
      "mall_id" : $("#mall_cd").val(),
      "csrfmiddlewaretoken": '{{ csrf_token }}'
    };
    mall_id = $("#mall_cd");


    if (mall_id.val() === "consolidado")
    {
      $("#show_door, #name_door").css('display', 'none');
      $("#show_door").prop("value","");
    }else{
      $("#show_door, #name_door").css('display', 'block');
    }


    //Funcion para la busqueda de las puertas de 1 o N centro comerciales

    $.ajax({
      data:  parametros,
      url:   '../../statistics/filter_door/',
      type:  'post',
      success:  function (response) {
        var data_select = JSON.parse(response);
        $("#show_door").html('<select id="door"></select>')
            //var select = $('#door');
            $('#door').multiselect({
              buttonWidth: '170%',
            });
            options = [];
            hash = {
              'value':'todas','title': 'Acceso', 'label': 'Todos los accesos'
            };
            options.push(hash);
            $.each(data_select, function( index, value ) {
              var valx = value['door_id'];
              var text = value['door_name'];
              hash = {
                'value':valx,'title': text, 'label': text
              };
              options.push(hash);                            
            });


            $('#door').multiselect('dataprovider', options);
          }
        });


  });
       //Funcion para el envio de data al grafico y al cuadro

       $("#print").click(function(){
        var parametros = {
          "mall_id" : $("#mall_cd").val(),
          "type_report" : $("#type_report").val(),
          "date1" : $("#date1").val(),
          "date2" : $("#date2").val(),
          "door": $("#door").val(),
          "csrfmiddlewaretoken": '{{ csrf_token }}'
        };


        $.ajax({
          data:  parametros,
          url:   '../../statistics/grafic/',
          type:  'post',
          success:  function (lista) {

            //para el grafico
            var data_reportes = JSON.parse(lista);
            var labels= [];
            var data = [];                            
            $.each(data_reportes, function(index, value){
              data.push(parseInt(value.y));
              labels.push(value.x);
            });
            removeData(myChart);
            addData(myChart,labels,data);


            //para el cuadro que muestra arriba del grafico

            $('#cuadro_detalle').html('');
            $.each(data_reportes, function( index, value ) {
              var x = value['x'];
              var y = value['y'];

              $("#cuadro_detalle").append("<tr class='odd'>");
              $("#cuadro_detalle").append("<td>"+x+"</td>");
              $("#cuadro_detalle").append("<td>"+y+"</td>");
              $('#cuadro_detalle').append('</tr>');
            });

          }

          
          
        });

      });


     });

