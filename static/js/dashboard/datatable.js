$(document).on('ready', function() {

$.ajax({
    url:   '../statistics/load_detail_table/',
    cache: false,
    beforeSend: function(){
                $('#detail_table').html("<div class='col-xs-16'><div class='load1 load-wrapper'><div class='loader'></div></div></div>");
                },
    success: function (response) {
        var data = JSON.parse(response);
        var data_table = (data.table);
        var data_mall = (data.mall);
        if (role != "1") {
            var data_door = (data.door);
        };
        var table = $('#detail_table');
        var select_mall = $('#mall_select');
        var select_door = $('#door_select');
        table.html("");
        select_mall.html("");
        // ---- TABLA ----
        table.append("<thead><tr>");
        table.append("<th>Fecha</th><th>Hora</th><th>Entrada</th><th>Salida</th>");
        table.append("</tr></thead>");
        $.each(data_table, function( index, value ) {
            table.append("<tbody><tr>");
            table.append("<td>"+value['date']+"</td>");
            table.append("<td>"+value['time']+"</td>");
            table.append("<td>"+value['sum_entry']+"</td>");
            table.append("<td>"+value['sum_exit']+"</td>");
            table.append("</tr></tbody>");
        });
        // ---- SELECT ----
        
        select_mall.css("display", "block");

        if (role == "1"){
            select_mall.append($('<option>', {
                    value: 0,
                    text: 'CONSOLIDADO',
                }));
            $.each(data_mall, function ( index, value ) {
                select_mall.append($('<option>', {
                    value: value.mall_id,
                    text: value.mall_name,
                }));
            });
        }else{
            $.each(data_mall, function ( index, value ) {
                select_mall.append($('<option>', {
                    value: value.mall_id,
                    text: value.mall_name,
                }));
            });

            select_door.css("display", "block");
            select_door.append($('<option>', {
                value: 0,
                text: 'TODAS',
            }));
            $.each(data_door, function ( index, value ) {
                select_door.append($('<option>', {
                    value: value.door_id,
                    text: value.door_name,
                }));
            });
        }
    },
});

  $('#detail_table').DataTable({
    responsive: true,
    pageLength:10,
    sPaginationType: "full_numbers",
    oLanguage: {
      oPaginate: {
        sFirst: "<<",
        sPrevious: "<",
        sNext: ">", 
        sLast: ">>" 
      }
    }
  });


});