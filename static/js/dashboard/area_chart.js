var chart;

    AmCharts.ready(function () {
        // SERIAL CHART
        chart = new AmCharts.AmSerialChart();
        chart.marginTop = 10;
        chart.categoryField = "hour";
        chart.fontFamily = "Arial"

        // AXES
        // Category
        var categoryAxis = chart.categoryAxis;
        categoryAxis.gridAlpha = 0.07;
        categoryAxis.axisColor = "#DADADA";
        categoryAxis.color = "#b7c8ff";
        categoryAxis.startOnAxis = true;

        // Value
        var valueAxis = new AmCharts.ValueAxis();
        // valueAxis.stackType = "regular"; // this line makes the chart "stacked"
        //valueAxis.gridAlpha = 0.07;
        valueAxis.gridThickness = 0;
        valueAxis.gridAboveGraphs = true,
        valueAxis.color ='#ffffff'; //PARA OCULTAR EL VALOR DEL AXIS Y
        valueAxis.axisColor = "#DADADA";
        valueAxis.titleColor ='white';
        valueAxis.title = "Entradas";
        valueAxis.titleFontSize = 20;


        chart.addValueAxis(valueAxis);

        // GRAPHS
        if (role == "1"){
            // first graph tolon_graph
            var tolon_graph = new AmCharts.AmGraph();
            tolon_graph.type = "line"; // it's simple line tolon_graph
            tolon_graph.title = "El Tolon";
            tolon_graph.fillColors = "#A6BE3D";
            tolon_graph.lineColor = "#A6BE3D";
            tolon_graph.valueField = "EL TOLON";
            tolon_graph.lineThickness = 5;
            tolon_graph.bullet = 'round';
            tolon_graph.lineAlpha = 1;
            tolon_graph.id = 1;
            tolon_graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area tolon_graph
            tolon_graph.balloonText = "<span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>";
            //graph.hidden = true;
            chart.addGraph(tolon_graph);


            // second graph hatillo_graph
            hatillo_graph = new AmCharts.AmGraph();
            hatillo_graph.type = "line";
            hatillo_graph.title = "Paseo El Hatillo";
            hatillo_graph.fillColors = "#0681C5";
            hatillo_graph.lineColor = "#0681C5";
            hatillo_graph.valueField = "PASEO EL HATILLO";
            hatillo_graph.bullet = 'round';
            hatillo_graph.lineThickness = 5;
            hatillo_graph.lineAlpha = 1;
            hatillo_graph.id = 2;
            hatillo_graph.fillAlphas = 0.1;
            hatillo_graph.balloonText = "<span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>";

            chart.addGraph(hatillo_graph);

            // third graphic hatillo_graph
            llano_mall_graph = new AmCharts.AmGraph();
            llano_mall_graph.type = "line";
            llano_mall_graph.title = "Llano Mall";
            llano_mall_graph.fillColors = "#DB5800";
            llano_mall_graph.lineColor = "#DB5800";
            llano_mall_graph.valueField = "LLANO MALL";
            llano_mall_graph.bullet = 'round';
            llano_mall_graph.lineThickness = 5;
            llano_mall_graph.lineAlpha = 1;
            llano_mall_graph.id = 3;
            llano_mall_graph.fillAlphas = 0.1;
            llano_mall_graph.balloonText = "<span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>";

            chart.addGraph(llano_mall_graph);
        }else{
            if(mall_role=='1'){
                var tolon_graph = new AmCharts.AmGraph();
                tolon_graph.type = "line"; // it's simple line tolon_graph
                tolon_graph.title = "El Tolon";
                tolon_graph.fillColors = "#A6BE3D";
                tolon_graph.lineColor = "#A6BE3D";
                tolon_graph.valueField = "EL TOLON";
                tolon_graph.lineThickness = 5;
                tolon_graph.bullet = 'round';
                tolon_graph.lineAlpha = 1;
                tolon_graph.id = 1;
                tolon_graph.fillAlphas = 0.1; // setting fillAlphas to > 0 value makes it area tolon_graph
                tolon_graph.balloonText = "<span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>";
                //graph.hidden = true;
                chart.addGraph(tolon_graph);
            }else if(mall_role=='2'){
                // second graph hatillo_graph
                hatillo_graph = new AmCharts.AmGraph();
                hatillo_graph.type = "line";
                hatillo_graph.title = "Paseo El Hatillo";
                hatillo_graph.fillColors = "#0681C5";
                hatillo_graph.lineColor = "#0681C5";
                hatillo_graph.valueField = "PASEO EL HATILLO";
                hatillo_graph.bullet = 'round';
                hatillo_graph.lineThickness = 5;
                hatillo_graph.lineAlpha = 1;
                hatillo_graph.id = 2;
                hatillo_graph.fillAlphas = 0.1;
                hatillo_graph.balloonText = "<span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>";
                chart.addGraph(hatillo_graph);
            }else if(mall_role=='3'){
                llano_mall_graph = new AmCharts.AmGraph();
                llano_mall_graph.type = "line";
                llano_mall_graph.title = "Llano Mall";
                llano_mall_graph.fillColors = "#DB5800";
                llano_mall_graph.lineColor = "#DB5800";
                llano_mall_graph.valueField = "LLANO MALL";
                llano_mall_graph.bullet = 'round';
                llano_mall_graph.lineThickness = 5;
                llano_mall_graph.lineAlpha = 1;
                llano_mall_graph.id = 3;
                llano_mall_graph.fillAlphas = 0.1;
                llano_mall_graph.balloonText = "<span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>";

                chart.addGraph(llano_mall_graph);
            }
        }


        // LEGEND
        var legend = new AmCharts.AmLegend();
        legend.position = "bottom";
        legend.color = "white";
        legend.valueText = "[[value]]";
        legend.valueWidth = 150;
        legend.valueAlign = "left";
        legend.align = "center";
        legend.fontSize = 17;
        legend.equalWidths = false;
        legend.periodValueText = "[[value.sum]]";
        legend.markerSize = 20; // this is displayed when mouse is not over the chart.
        chart.addLegend(legend);

        // CURSOR
        var chartCursor = new AmCharts.ChartCursor();
        chartCursor.cursorAlpha = 0;
        chart.addChartCursor(chartCursor);

        // WRITE
        //chart.write("chartdiv");


        $.ajax({
          url:   '../statistics/load_chart_hours_blocks/',
          cache: false,
          beforeSend: function(){
            $('#chartdiv').html("<div class='col-xs-16'><div class='load1 load-wrapper'><div class='loader'></div></div></div>");
            $("#freq-button").css("display", "none");
          },
          success:  function (response) {

            chart.dataProvider = JSON.parse(response);

            chart.write("chartdiv");
            chart.validateData();
            $("#freq-button").css("display", "block");
          },
        });
    });
